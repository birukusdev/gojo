#!/bin/bash

# dependencies - GCC,G++, Python2.7, Cython

CONDA=/opt/anaconda2
PYTHON=$CONDA/bin/python
CYTHON=$CONDA/bin/cython

PYZWAVEDIR=py-open-zwave

# First compile the MakeFile

cd $PYZWAVEDIR


######################
#
#
################

sudo apt-get install g++ libudev-dev

######################

# Make Build - not install
make build

# Make docs
make docs