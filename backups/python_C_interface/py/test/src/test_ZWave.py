#
import unittest
import time
from zwave.ZWave import ZWave

class Test_ZWave(unittest.TestCase):

    zw  = 0

    def setup(self):

        libpath = "/Users/btesfaye/Birukus/dev/gojo/gitmaster/dev/server/drivers/ZWaveInterface/bin/libgojo-zwave-interface2.so"

        print("--starting zwave--")
        self.zw = ZWave(libpath)

    def test_init(self):
        libpath = "/Users/btesfaye/Birukus/dev/gojo/gitmaster/dev/server/drivers/ZWaveInterface/bin/libgojo-zwave-interface2.so"

        print("--starting zwave--")
        self.zw = ZWave(libpath)

        configpath = "/Users/btesfaye/Birukus/dev/gojo/gitmaster/lib/share/open-zwave-build/share/config"
        usbpath    = "/dev/tty.usbmodem1421"
        self.zw.initDriver(configpath,usbpath)

class Test_ZWave2:

    def __init__(self):
        libpath = "/Users/btesfaye/Birukus/dev/gojo/gitmaster/dev/server/drivers/ZWaveInterface/bin/libgojo-zwave-interface2.so"

        print("--starting zwave--")
        self.zw = ZWave(libpath)

    def test_init(self):
        configpath = '/Users/btesfaye/Birukus/dev/gojo/gitmaster/lib/share/open-zwave-build/share/config'
        usbpath    = '/dev/cu.usbmodem1421'

        print(":::testing_init")
        self.zw.initDriver(configpath.encode('utf-8'),usbpath.encode('utf-8'))
        print('----<close init>----')
    def wait(self, sec):
        time.sleep(sec)

    def shutdown(self):
        print(':::testing_shutdown')
        self.zw.shutdownDriver()
        print('---<close shutdown>---')

if __name__ == '__main__':

    #print("--ZWave Test--")
    #unittest.main()

    ##### ZWave Test ######
    zw = Test_ZWave2()

    zw.test_init()

    zw.wait(300)

    zw.shutdown()