#
import unittest
import time
from controller import Controller
from threading import Thread

class Test_Controller(unittest.TestCase):

    zw  = 0

    def setup(self):
        self.libpath    = "/Users/btesfaye/Birukus/dev/gojo/gitmaster/dev/server/drivers/ZWaveInterface/bin/libgojo-zwave-interface2.so"
        self.configpath = '/Users/btesfaye/Birukus/dev/gojo/gitmaster/lib/share/open-zwave-build/share/config'
        self.usbpath    = '/dev/cu.usbmodem1421'
        self.procConfig = ''

    def test_main(self):
        self.libpath    = "/Users/btesfaye/Birukus/dev/gojo/gitmaster/dev/server/drivers/ZWaveInterface/bin/libgojo-zwave-interface2.so"
        self.configpath = '/Users/btesfaye/Birukus/dev/gojo/gitmaster/lib/share/open-zwave-build/share/config'
        self.usbpath    = '/dev/cu.usbmodem1421'
        self.procConfig = ''

        # start the controller
        cc = Controller()

        # init the zwave
        cc.initZwave(self.libpath,
                     self.configpath,
                     self.usbpath,
                     self.procConfig)

        # run zwave
        cc.runZwave()

if __name__ == '__main__':
    libpath    = "/Users/btesfaye/Birukus/dev/gojo/gitmaster/dev/server/drivers/ZWaveInterface/bin/libgojo-zwave-interface2.so"
    configpath = "/Users/btesfaye/Birukus/dev/gojo/gitmaster/lib/share/open-zwave-build/share/config"
    usbpath    = "/dev/cu.usbmodem1421"
    procConfig = ''

    # start the controller
    cc = Controller()
    print("---init---")
    # init the zwave
    cc.initZwave(libpath,
                 configpath,
                 usbpath,
                 procConfig)

    cc.runZwave()
    # thread = Thread(target = cc.runZwave, args = ( ))
    # thread.start()
    # thread.join()
    #
    # print(">>>>>>>>waiting>>>>>>>>>")
    # for n in range(0,200):
    #     time.sleep(5)
    #     print("%%% waiting %%%")

