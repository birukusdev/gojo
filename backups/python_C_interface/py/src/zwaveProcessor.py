
'''
    This will input
    The data from the ZWaveDriver and store it
'''
from nodeInputBuffer import NodeData,NodeInputBuffer

from nodeDataProcessor import NodeDataProcessor

import time

class ZWaveProcessor:
    '''
    This class loads the data from the ZWaveDriver,
        * load data
        * store sensor data in array
        * processdd the data in the array
        * for every even, take action
            * output signals
            * send message to users
    '''

    def __init__(self,zwaveLibpath,zwaveConfigPath,usbPort,procConfigPath):

        self.inputBuffer  = NodeInputBuffer(zwaveLibpath,zwaveConfigPath,usbPort)

        self.nodeProcessor= NodeDataProcessor(procConfigPath)

        self.runProcessor = True

    def stop(self):
        self.runProcessor = False

    def run(self):

        print("---zwaveProcessor::run()")
        # start a loop that listens to the input
        while (self.runProcessor):

            # ask driver for new data
            self.inputBuffer.queryDriverForData()

            # check if there is data in the input buffer
            [nodeValid, nodeData] = self.inputBuffer.getNode()

            if nodeValid: # node valid

                self.nodeProcessor.process(nodeData)

            print("...")
            time.sleep(1)

        print("... zwaveProcessor-loop over")