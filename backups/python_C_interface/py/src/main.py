#!/usr/bin/python

#
#
#   Gojo: House Automation Software
#
#

from controller import Controller

libpath    = "/Users/btesfaye/Birukus/dev/gojo/gitmaster/dev/server/drivers/ZWaveInterface/bin/libgojo-zwave-interface2.so"
configpath = '/Users/btesfaye/Birukus/dev/gojo/gitmaster/lib/share/open-zwave-build/share/config'
usbpath    = '/dev/cu.usbmodem1421'
procConfig = ''

if __name__=="__main__":
    # start the controller
    cc = Controller()
    # init the zwave
    cc.initZwave(libpath,configpath,usbpath,procConfig)
    # run zwave
    cc.runZwave()



