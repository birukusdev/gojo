'''
    Node processor
'''

from zwave.NodeData import NodeData
class NodeDataProcessor:
    '''
        This class processes Node data
    '''
    def __init__(self,configFile):
        '''
        loads the config file
        :return:
        '''

    def __del__(self):
        print("--NodeDataProcessor- destructor")

    def process(self,node):
        '''
            Process Node data
        :param nodeData:
        :return:
        '''
        node.print()