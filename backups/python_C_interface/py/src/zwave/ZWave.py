import ctypes
from zwave.NodeData import NodeData
import os.path
from util.util import *

'''
    ZWave class:
        This class creates an interface with the c++ zwave-driver

'''
class ZWave:

    libraryPath = ""

    def __init__(self, libpath):
        self.libraryPath = libpath

        # check if file exists
        if not os.path.isfile(libpath):
            throwError("pyZWave Driver: given C++ *.so file cannot be found filename= "+libpath)

        # load the library
        self.zw = ctypes.cdll.LoadLibrary(libpath)

        # function pointers
        self.func_initDriver = self.zw.initDriver
        self.func_initDriver.restype  = ctypes.c_bool
        self.func_initDriver.argtypes = [ctypes.c_char_p, ctypes.c_char_p]

        self.func_shutdownDriver = self.zw.shutdownDriver
        self.func_shutdownDriver.restype = ctypes.c_bool

        self.func_getNodeChangeHistorySize = self.zw.getNodeChangeHistorySize
        self.func_getNodeChangeHistorySize.restype = ctypes.c_int

        self.func_popNodeChangeHistoryData = self.zw.popNodeChangeHistoryData
        self.func_popNodeChangeHistoryData.restype = ctypes.POINTER(NodeData)

        self.func_setNodeDataStr = self.zw.setNodeDataStr
        self.func_setNodeDataStr.restype  = ctypes.c_bool
        self.func_setNodeDataStr.argtypes = [ctypes.c_int, ctypes.c_int, ctypes.c_char_p]

        self.func_setNodeDataInt = self.zw.setNodeDataInt
        self.func_setNodeDataInt.restype = ctypes.c_int
        self.func_setNodeDataInt.argtypes= [ctypes.c_int, ctypes.c_int, ctypes.c_int]

        self.func_setNodeDataFloat = self.zw.setNodeDataFloat
        self.func_setNodeDataFloat.restype = ctypes.c_float
        self.func_setNodeDataFloat.argtypes= [ctypes.c_int, ctypes.c_int, ctypes.c_float]

        self.func_setNodeDataBool = self.zw.setNodeDataBool
        self.func_setNodeDataBool.restype = ctypes.c_bool
        self.func_setNodeDataBool.argtypes= [ctypes.c_int, ctypes.c_int, ctypes.c_bool]

        self.func_getNodeData = self.zw.getNodeData
        self.func_getNodeData.restype = ctypes.POINTER(NodeData)
        self.func_getNodeData.argtypes= [ctypes.c_int, ctypes.c_int]

        self.func_getNodeStrData = self.zw.getNodeStrData
        self.func_getNodeStrData.restype = ctypes.c_char_p
        self.func_getNodeStrData.argtypes= [ctypes.c_int, ctypes.c_int]

        self.func_getNodeIntData = self.zw.getNodeStrData
        self.func_getNodeIntData.restype = ctypes.c_int
        self.func_getNodeIntData.argtypes= [ctypes.c_int, ctypes.c_int]

        self.func_getNodeBoolData = self.zw.getNodeBoolData
        self.func_getNodeBoolData.restype = ctypes.c_bool
        self.func_getNodeBoolData.argtypes= [ctypes.c_int, ctypes.c_int]

        self.func_getNodeLayout = self.zw.getNodeLayout
        self.func_getNodeLayout.restype = ctypes.c_char_p
        self.func_getNodeLayout.argtypes= [ctypes.c_int, ctypes.c_int]


    def initDriver(self, aConfigFilePath, aUSBPort): #bool
        # init the driver
        return self.func_initDriver(aConfigFilePath.encode("UTF-8"), aUSBPort.encode("UTF-8"))

    def shutdownDriver(self): #bool
        return self.func_shutdownDriver()

    def getNodeChangeHistorySize(self): #int
        return self.func_getNodeChangeHistorySize

    def popNodeChangeHistoryData(self): #NodeData
        return self.func_popNodeChangeHistoryData()

    def getNodeData(self, nodeid, classid): #NodeData
        return self.func_getNodeData(nodeid,classid)

    def setNodeDataStr(self, nodeid, classid, value): #bool
        return self.func_setNodeDataStr(nodeid,classid, value)

    def setNodeDataInt(self, nodeid, classid, value): #bool
        return self.func_setNodeDataInt(nodeid,classid, value)

    def setNodeDataFloat(self, nodeid, classid, value): #bool
        return self.func_setNodeDataFloat(nodeid,classid, value)

    def setNodeDataBool(self, nodeid, classid, value): #bool
        return self.func_setNodeDataBool(nodeid,classid, value)

    def getNodeStrData(self, nodeid, classid): #str
        return self.func_getNodeStrData(nodeid,classid)

    def getNodeIntData(self, nodeid, classid): #int
        return self.func_getNodeIntData(nodeid,classid)

    def getNodeBoolData(self, nodeid, classid): #bool
        return self.func_getNodeBoolData(nodeid,classid)

    def getNodeLayout(self): #str
        return self.func_getNodeLayout()




#
#
#
# libpath="/Users/btesfaye/Birukus/dev/gojo/gitmaster/dev/server/drivers/ZWaveInterface/bin/libgojo-zwave-interface2.so"
#
# zw = ctypes.cdll.LoadLibrary(libpath)
#
# prototype = ctypes.CFUNCTYPE(
#     ctypes.c_char_p,
#     ctypes.c_char_p,
#     ctypes.c_char_p
# )
# testfunc = prototype(('test2', zw))
#
# res = testfunc(b"Hello, %s\n",b"Hello, %s\n" )
# print(res)
#
# class NodeData(ctypes.Structure):
#     _fields_ = [("isInit", ctypes.c_bool),
#                 ("timeStr",ctypes.c_char_p ),
#                 ("homeid", ctypes.c_int ),
#                 ("nodeid", ctypes.c_int ),
#                 ("valueType", ctypes.c_char_p),
#                 ("valueLabel",ctypes.c_char_p),
#                 ("valueUnits",ctypes.c_char_p),
#                 ("valueHelp", ctypes.c_char_p),
#                 ("valueMin",ctypes.c_int),
#                 ("valueMax",ctypes.c_int),
#                 ("valueStr",ctypes.c_char_p),
#                 ("valueInt",ctypes.c_int),
#                 ("valueBool",ctypes.c_bool),
#                 ("valueBool",ctypes.c_float)
#                 ]
#
# lib = ctypes.cdll.LoadLibrary(libpath)
# lib.test2.restype = ctypes.POINTER(NodeData)
# lib.test2.argtypes = [ctypes.c_int]
# #p1 = ctypes.create_string_buffer('param1')
# #p2 = ctypes.create_string_buffer('param2')
# s = lib.test2(555);
# #print(s)
