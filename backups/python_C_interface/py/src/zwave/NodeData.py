import ctypes

'''
    NodeData is used by the python-zwave driver
        This class is a struct definition of the C++ class named NodeData
'''
class NodeData(ctypes.Structure):
    _fields_ = [("isInit", ctypes.c_bool),
                ("timeStr", ctypes.c_char_p ),
                ("homeid", ctypes.c_int ),
                ("nodeid", ctypes.c_int ),
                ("commandClassid", ctypes.c_uint8),
                ("nodetype", ctypes.c_uint8),
                ("valueType", ctypes.c_char_p),
                ("valueLabel", ctypes.c_char_p),
                ("valueUnits", ctypes.c_char_p),
                ("valueHelp", ctypes.c_char_p),
                ("valueMin", ctypes.c_int),
                ("valueMax", ctypes.c_int),
                ("valueStr", ctypes.c_char_p),
                ("valueInt", ctypes.c_int),
                ("valueBool", ctypes.c_bool),
                ("valueFloat", ctypes.c_float)
                ]

    def get(self,key):

        data=0
        found=False
        for n in range(0,len(self._fields_)):
            if self._fields_[n][0]==key:
                data=self._fields_[n][1]
                found=True
                break

        return [found,data]

    def print(self):
        print("-----Node valid("+self.get("isInit")[1].value+")")
        print("  time:  "+self.get("timeStr")[1].value)
        print("  homeid:"+self.get("homeid")[1].value)
        print("  nodeid:"+self.get("nodeid")[1].value)
        print("  commandClassid:"+self.get("commandClassid")[1].value)
        print("  nodetype: "+self.get("nodetype")[1].value)
        print("  valueType:"+self.get("valueType")[1].value)
        print("  valueInt: "+self.get("valueInt")[1].value)
        print("  valueStr: "+self.get("valueStr")[1].value)