
from zwaveProcessor import ZWaveProcessor


class Controller:
    '''
        Main Zwave Controller
    '''

    def __init__(self):
        '''
            Start the controller
        :return:
        '''

    def __del__(self):
        print("Controller: shutting down runProcessor")
        self.zwproc.runProcessor = False

    def initZwave(self,zwaveLibpath,zwaveConfigPath,usbPort,procConfigPath):
        '''
            Start the Zwave driver
        :param zwaveLibpath:
        :param zwaveConfigPath:
        :param usbPort:
        :param procConfigPath:
        :return:
        '''
        self.zwproc = ZWaveProcessor(zwaveLibpath,zwaveConfigPath,usbPort,procConfigPath)

    def runZwave(self):
        '''
            run the zwave
        :return:
        '''
        self.zwproc.run()




