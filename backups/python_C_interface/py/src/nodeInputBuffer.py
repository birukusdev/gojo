'''
    This stores all the node input data into an accessible array
'''

from zwave.ZWave import ZWave

class NodeData:
    classID=0
    nodeID=0
    homeID=0
    eventTime=0
    nodeData=''

    def __init__(self):
        self.eventTime=0

    def __init__(self,aClassID,aNodeID,aHomeID,aTime,aNodeData):
        self.classID=aClassID
        self.nodeID=aNodeID
        self.homeID=aHomeID
        self.eventTime=aTime
        self.nodeData=aNodeData

class NodeInputBuffer:
    '''
        Inputs all the node data
    '''
    nodeBufferHistory=[]
    nodeBuffer=[]

    def __init__(self, libpath,configPath,usbPort):
        '''
            Set up the Input Buffer
        :return:
        '''
        # init the zwave driver
        self.zw = ZWave(libpath)

        self.zw.initDriver(configPath, usbPort)

    def __del__(self):
        '''
        Destructor
        :return:
        '''

        print("--NodeInputBuffer: shutdown zwave")
        self.zw.shutdownDriver()

    def addNode(self,node):
        self.nodeBufferHistory.append(node)
        self.nodeBuffer.append(node)

    def getNode(self):
        if len(self.nodeBuffer)>0:
            return [True,self.nodeBuffer.pop()]
        # if there are no more nodes
        return [False, 0]

    def queryDriverForData(self):
        '''
        This gets the node data from the zwave-driver
        :return:
        '''

        print("queryDriveForData")
        data = self.zw.popNodeChangeHistoryData()
        print("test2")
        print(dir(data))
        data.contents
        valid,isInit = data.contents.get("isInit")

        data.contents.print()

        if valid==True and isInit==True:
            n = NodeData()
            _,n.classID = data.get("commandClassid")
            _,n.nodeID = data.get("nodeid")
            _,n.homeID = data.get("homeid")
            _,nodeType = data.get("nodetype")
            _,n.nodeData = data.get("valueStr")

            # add this node to the buffer
            self.nodeBuffer.append(n)
            # add this node to history
            self.nodeBufferHistory.append(n)

