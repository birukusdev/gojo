/*
 * main.cpp
 *
 *  Created on: Jan 18, 2016
 *      Author: btesfaye
 */

#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds
#include "zwave/DataTypes.h"
#include "zwave/zwavedriver.h"
#include "zwave/DataTypes.h"

#define DEBUG 1

bool initDriver(char* anConfigFilePath,char* aUSBPort)
{
	Gojo::Zwave::ZwaveDriver::getInstance()->init(std::string(aUSBPort),std::string(anConfigFilePath) );

	//isDriverInitialized = true;
	return true;
}

bool shutdownDriver()
{
	Gojo::Zwave::ZwaveDriver::getInstance()->~ZwaveDriver();
	return false;
}

int getNodeChangeHistorySize(){
	return Gojo::Zwave::ZwaveDriver::getInstance()->getNodeChangeHistorySize();
}

	// get the message
	  Gojo::Zwave::SimpleNodeData popNodeChangeHistoryData(){
		  printf("ren and stimpy\n");
		return Gojo::Zwave::ZwaveDriver::getInstance()->popNodeChangeHistory();
	}

	  bool setNodeDataStr(int nodeid, int classid, char* value){
		  return Gojo::Zwave::ZwaveDriver::getInstance()->setNodeValue(nodeid,classid,std::string(value)); }

	  bool setNodeDataInt(int nodeid, int classid, int value){
		return Gojo::Zwave::ZwaveDriver::getInstance()->setNodeValue(nodeid,classid,(int32)value); }

	  bool setNodeDataFloat(int nodeid, int classid, float value){
		return Gojo::Zwave::ZwaveDriver::getInstance()->setNodeValue(nodeid,classid,value); }

	  bool setNodeDataBool(int nodeid, int classid, bool value){
		return Gojo::Zwave::ZwaveDriver::getInstance()->setNodeValue(nodeid,classid,value); }


	  Gojo::Zwave::SimpleNodeData getNodeData(int nodeid, int classid){
		return Gojo::Zwave::ZwaveDriver::getInstance()->getNodeValue(nodeid,classid);
	  }

	  const char* getNodeStrData(int nodeid, int classid){
		return Gojo::Zwave::ZwaveDriver::getInstance()->getNodeValue(nodeid,classid).valueStr; }

	  int getNodeIntData(int nodeid, int classid){
		return Gojo::Zwave::ZwaveDriver::getInstance()->getNodeValue(nodeid,classid).valueInt; }

	  bool getNodeBoolData(int nodeid, int classid){
		return Gojo::Zwave::ZwaveDriver::getInstance()->getNodeValue(nodeid,classid).valueBool; }

	const char* getNodeLayout(){
		return Gojo::Zwave::ZwaveDriver::getInstance()->getNodeLayout().c_str(); }


int testprint(char* msg)
{
    return printf("%s",msg);
}

int main()
{

	printf("---ZWaveDriver - test routine \n");

	char configpath[] = "/Users/btesfaye/Birukus/dev/gojo/gitmaster/lib/share/open-zwave-build/share/config";
	char usbpath[]   = "/dev/cu.usbmodem1421";
	initDriver( configpath, usbpath );
	printf("---driver initialized \n");

	std::this_thread::sleep_for (std::chrono::seconds(20));

	//printf("%s\n",getNodeLayout().c_str());

	for(int n=0; n<10; n++)
	{
		std::this_thread::sleep_for (std::chrono::seconds(20));
		printf("..\n");
	}

	std::this_thread::sleep_for (std::chrono::seconds(20));

	shutdownDriver();

	return 0;
}



