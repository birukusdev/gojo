/*!
*
*   Birukus ( www.birukus.com )
*
*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
*
*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
*
*   Date: 09/2015
*
**/

#ifndef ZWAVEDRIVER_H
#define ZWAVEDRIVER_H

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <list>
#include <stdlib.h>
#include <string>
#include <iostream>

#include "openzwave/Options.h"
#include "openzwave/Manager.h"
#include "openzwave/Driver.h"
#include "openzwave/Node.h"
#include "openzwave/Group.h"
#include "openzwave/Notification.h"
#include "openzwave/value_classes/ValueStore.h"
#include "openzwave/value_classes/Value.h"
#include "openzwave/value_classes/ValueBool.h"
#include "openzwave/platform/Log.h"

#include "singleton.h"
#include "DataTypes.h"

namespace Gojo{
	namespace Zwave{
		
#ifndef DEBUG
#define DEBUG 1
#endif

/**
 * @brief The ZwaveController class
 * This class controlls the incoming/outgoing signals from the USB hub
 */
class ZwaveDriver : public Singleton<ZwaveDriver>
{
	friend class Singleton<ZwaveDriver>;

private: // Z-wave specific

	// which USB Port to use
	std::string m_USBPort;

	// xml device config path
	std::string m_XMLConfigPath;

	// home id
	uint32 m_homeId; // TODO change

	// init status
	bool   m_initFailed = false;

	// stores the list of nodes on a device
	std::list<NodeInfo*> m_nodes;

	// used to lock threads
	pthread_mutex_t m_procMutex;
	// mutex used in init
	pthread_cond_t  m_initCond  = PTHREAD_COND_INITIALIZER;
	pthread_mutex_t m_initMutex = PTHREAD_MUTEX_INITIALIZER;


private: // variables

	bool isInitialized = false;

	std::list<std::string> m_Log;

	std::list<Gojo::Zwave::SimpleNodeData> nodeChangeHistory;

private: // methods

	ZwaveDriver();

	void initZWave();

	void shutdownZWave();

	void initPThreads();

	void lockThread();

	void unlockThread();

	NodeInfo* GetNodeInfo(OpenZWave::Notification const* _notification);

	// writes log message to array
	void writeLog(std::string msg){
		std::string logmsg = getTime() + "--zwave-driver: " + msg;

		if(DEBUG)
			printf("%s\n",logmsg.c_str());

		m_Log.push_front(logmsg);
	}

	// print time in string format
	std::string getTime(){
		  time_t rawtime;
		  struct tm * timeinfo;

		  time (&rawtime);
		  timeinfo = localtime (&rawtime);
		  return asctime(timeinfo);
	}

public: // public methods

	~ZwaveDriver();

	void init(std::string aUSBPort, std::string aXMLConfigPath);

	static void zwaveManagerCallBack( OpenZWave::Notification const* _notification, void* _context );

	bool setNodeValue(int nodeid, int classid, const bool value);
	bool setNodeValue(int nodeid, int classid, const char* value);
	bool setNodeValue(int nodeid, int classid, const std::string value);
	bool setNodeValue(int nodeid, int classid, float value);
	bool setNodeValue(int nodeid, int classid, int32 value);
	bool setNodeValue(int nodeid, int classid, unsigned short value);
    // bool, str, float, int short char

	/**
	 * @brief Gojo::Zwave::ZwaveController::getDeviceValue
	 * @param nodeid  - device id
	 * @return   value of the device id
	 */
	Gojo::Zwave::SimpleNodeData getNodeValue(int nodeid, int classid);

	// this returns the whole log and clears the array
	std::string getLog()
	{
	    pthread_mutex_lock( &m_procMutex ); // obtain lock before processing

		std::string log;
		for( std::list<std::string>::iterator it = m_Log.begin(); it != m_Log.end(); ++it )
				log += *it + "\n";
		m_Log.clear();

		pthread_mutex_unlock( &m_procMutex );

		return log;
	}

	int getNodeChangeHistorySize(){
		return nodeChangeHistory.size();
	}

	Gojo::Zwave::SimpleNodeData popNodeChangeHistory(){

		printf("--DEBUG:popNodeChangeHistory- started");
		Gojo::Zwave::SimpleNodeData data; data.isInit = false;
		pthread_mutex_lock( &m_procMutex ); // obtain lock before processing
		if(nodeChangeHistory.size()>0){

			printf("--DEBUG:popNodeChangeHistory - changehistory>0 ");
			data = nodeChangeHistory.front();
			nodeChangeHistory.pop_front();
		}
		pthread_mutex_unlock( &m_procMutex );
		return data;
	}

	std::string getNodeLayout();
};
		
		
	}
}
#endif // ZWAVE_H

