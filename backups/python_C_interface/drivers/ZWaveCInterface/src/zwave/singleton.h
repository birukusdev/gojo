/*!
*
*   Birukus ( www.birukus.com )
*
*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
*
*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
*
*   Date: 09/2015
*
*/

/*!
 * \brief Singleton Template class
 *
 *  The Singleton class allows inheriting classes to become singletons.
 *  The class also prevents the inheriting classes from being copied.
 *  This class is used for memory-intensive applications
 */
#ifndef SINGLETON_H_
#define SINGLETON_H_

#include <memory> // unique_ptr, reset
#include <mutex>  // once_flag


    /*!
     * \brief Prevents inheriting classes from being copied
     *
     */
    namespace noncopyable_  // protection from unintended ADL
    {
      class noncopyable
      {
       protected:
          noncopyable() {}
          ~noncopyable() {}
       private:  // emphasize the following members are private
          //noncopyable( const noncopyable& );
          const noncopyable& operator=( const noncopyable& );
      };
    }

    typedef noncopyable_::noncopyable noncopyable;

    /*! \brief The Singleton class allows inheriting classes to become singletons.
     *
     */
    template <class T>
    class Singleton : private noncopyable
    {
    private:
        static std::once_flag& getOnce()
        {  static std::once_flag mOnce;  return mOnce;  }

        static std::unique_ptr<T> mInstance;

    public:

        static T* getInstance()
        {
           // use call once to create one instance
           std::call_once( getOnce(), [](){ mInstance.reset(new T()); } );
           // return the existing instance for consequitive calls
           return mInstance.get();
        }

        static T* ptr()
        {
            return getInstance();
        }

    protected:
        explicit Singleton<T>() {}

    };

  template<class T> std::unique_ptr<T> Singleton<T>::mInstance = nullptr;

#endif /* SINGLETON_H_ */


/**
  Example: how to use the singleton class

    class Logger : public Singleton<Logger>
    {
            friend class Singleton<Logger>;

        private:

        ...
    }


    &Logger

        **/

