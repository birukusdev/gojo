/*
 * DataTypes.h
 *
 *  Created on: Jan 18, 2016
 *      Author: btesfaye
 */

#ifndef SRC_ZWAVE_DATATYPES_H_
#define SRC_ZWAVE_DATATYPES_H_


namespace Gojo{
	namespace Zwave{

		struct NodeData
		{
			bool               isInit =  false;
			const OpenZWave::ValueID* valueID;
			uint8              commandClassID;
			uint8              nodeType;

			std::string        valueLabel;
			std::string        valueUnits;
			std::string        valueHelp;
			int                valueMin;
			int                valueMax;
			std::string        valueStr;
			int                valueInt;
			bool               valueBool;
			float              valueFloat;
		public:
			NodeData(){}
		};

		extern "C"
		struct SimpleNodeData
		{
			bool               	isInit =  false;
			char* 				timeStr;

			int					homeid;
			int					nodeid;

			uint8              	commandClassID;
			uint8              	nodeType;

			const char*			valueType;
			const char*       	valueLabel;
			const char*        	valueUnits;
			const char*        	valueHelp;
			int                	valueMin;
			int                	valueMax;

			const char*        	valueStr;
			int                	valueInt;
			bool               	valueBool;
			float              	valueFloat;

			bool set(std::string time, int aHomeID, int aNodeID)
			{
				//timeStr = time.c_str();
				homeid  = aHomeID;
				nodeid  = aNodeID;

				return true;
			}
			bool set(NodeData node)
			{

				commandClassID = node.commandClassID;
				nodeType = node.nodeType;
				valueLabel = node.valueLabel.c_str();
				valueUnits = node.valueUnits.c_str();
				valueHelp = node.valueHelp.c_str();
				valueMin = node.valueMin;
				valueMax = node.valueMax;
				valueStr = node.valueStr.c_str();
				valueInt = node.valueInt;
				valueBool = node.valueBool;
				valueFloat = node.valueFloat;

				return true;
			}
		} ;

		struct NodeInfo
		{
			uint32		m_homeId;
			uint8		m_nodeId;
			bool		m_polled;
			std::list<NodeData>	m_values;
		} ;



	}
}


#endif /* SRC_ZWAVE_DATATYPES_H_ */
