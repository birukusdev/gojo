/**
*
*	zwave: interface for openzwave
*
*
**/

#ifndef ZWAVE0_H
#define ZWAVE0_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <string>
#include <iostream>
#include "zwave/zwavedriver.h"
#include "zwave/DataTypes.h"

/**
 *
 *	ZWave: this class provides a static function interface for the python program to call
 *
 **/

#ifndef DEBUG
#define DEBUG 1
#endif


/**
 *	Initialize the driver using the xml config file
 *
 **/

//// set the node value
//template<class T>
//static bool setNodeDataTemplate(int nodeid, int classid, T value){
//	return Gojo::Zwave::ZwaveDriver::getInstance()->setNodeValue(nodeid,classid,value);
//}
//
//extern "C" {
//
//	 bool initDriver(char* anConfigFilePath,char* aUSBPort)
//	{
//		Gojo::Zwave::ZwaveDriver::getInstance()->init(std::string(aUSBPort),std::string(anConfigFilePath) );
//
//		//isDriverInitialized = true;
//		return true;
//	}
//
//	 bool shutdownDriver()
//	{
//		Gojo::Zwave::ZwaveDriver::getInstance()->~ZwaveDriver();
//		return false;
//	}
//
//	 int getNodeChangeHistorySize(){
//		return Gojo::Zwave::ZwaveDriver::getInstance()->getNodeChangeHistorySize();
//	}
//
//	// get the message
//	static Gojo::Zwave::SimpleNodeData popNodeChangeHistoryData(){
//		return Gojo::Zwave::ZwaveDriver::getInstance()->popNodeChangeHistory();
//	}
//
//	 bool setNodeDataStr(int nodeid, int classid, char* value){
//		 return setNodeDataTemplate(nodeid,classid,std::string(value)); }
//
//	 bool setNodeDataInt(int nodeid, int classid, int value){
//		 return setNodeDataTemplate(nodeid,classid,value); }
//
//	 bool setNodeDataFloat(int nodeid, int classid, float value){
//		 return setNodeDataTemplate(nodeid,classid,value); }
//
//	 bool setNodeDataBool(int nodeid, int classid, bool value){
//		 return setNodeDataTemplate(nodeid,classid,value); }
//
//
//	 Gojo::Zwave::SimpleNodeData getNodeData(int nodeid, int classid){
//		return Gojo::Zwave::ZwaveDriver::getInstance()->getNodeValue(nodeid,classid);
//	}
//
//	 const char* getNodeStrData(int nodeid, int classid){
//		return Gojo::Zwave::ZwaveDriver::getInstance()->getNodeValue(nodeid,classid).valueStr; }
//
//	 int getNodeIntData(int nodeid, int classid){
//		return Gojo::Zwave::ZwaveDriver::getInstance()->getNodeValue(nodeid,classid).valueInt; }
//
//	 bool getNodeBoolData(int nodeid, int classid){
//		return Gojo::Zwave::ZwaveDriver::getInstance()->getNodeValue(nodeid,classid).valueBool; }
//
//	 const char* getNodeLayout(){
//		 return Gojo::Zwave::ZwaveDriver::getInstance()->getNodeLayout().c_str(); }
//
//}
//

#endif
