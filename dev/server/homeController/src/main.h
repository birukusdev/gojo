/*!
*
*   Birukus ( www.birukus.com )
*
*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
*
*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
*
*   Date: 09/2015
*
*/
#ifndef MAIN_H
#define MAIN_H

#include "drivers/zwave/zwavedriver.h"

bool run();

#endif // MAIN_H

