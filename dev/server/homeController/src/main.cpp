/*!
*
*   Birukus ( www.birukus.com )
*
*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
*
*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
*
*   Date: 09/2015
*
*/
#undef HAVE_GAS_HIDDEN

#if __cplusplus <= 199711L //Test the compiler to make sure it is c++11 compliant
#error PGSUS: Compiler Error - This Program needs at least a C++11 compliant compiler
#endif

// C++ system includes
// QT includes
#include <QApplication>

// Gojo Includes
#include "drivers/ui/gui/mainwindow.h"
#include "drivers/zwave/zwavedriver.h"
#include "util/logger.h"
#include "util/error.h"
#include "util/settings.h"
#include "controllers/controller.h"
#include "main.h"

int main(int argc, char *argv[])
{

  // register the system errors
  Gojo::Util::Error::initSystemErrorHandle();


  //throw a welcome message
  printf("              ░░░░░                \n");
  printf("            ░░░░░░░░░                \n");
  printf("          ░░░░░░░░░░░░░░               \n");
  printf("        ░░░░░░░░░░░░░▒░░░░░       *      \n");
  printf("      ═══════════════════════              \n");
  printf("    ░░░░░░░░░░▒▒░░░░▒▒░░░░░░░░░              \n");
  printf("  ░░░░░░░░░░░▒▒▒▒▒░░▒▒▒░░░░░░░░░░░             \n");
  printf("░░░░░░░░▒▒▒▒▒▒░░░░░░▒▒▒▒▒░░░░░░░░░░░░░           \n");
  printf("░  ██████╗  ██████╗      ██╗ ██████╗ ░           \n");
  printf("░ ██╔════╝ ██╔═══██╗     ██║██╔═══██╗░           \n");
  printf("░ ██║  ███╗██║   ██║     ██║██║   ██║░      *    \n");
  printf("░ ██║   ██║██║   ██║██   ██║██║   ██║░     _O_   \n");
  printf("░ ╚██████╔╝╚██████╔╝╚█████╔╝╚██████╔╝░    / | \\  \n");
  printf("░  ╚═════╝  ╚═════╝  ╚════╝  ╚═════╝ ░     / \\   \n");
  printf("░▒░▒░▒░▒░▒░▒░▒░▒░▒░▒░▒░▒░▒░▒░▒░▒░▒░▒░░░▒░▒░░▒░▒░░▒░▒░\n");

  // Check the number of parameters
  if (argc != 2) {
      // Help information on how to run the program
      std::cerr << "Gojo: -  "<<std::endl<<std::endl;
      std::cerr << "Help Information  (user given arguments = " <<argc<<" )"<< std::endl;
      std::cerr << "NAME  " << std::endl;
      std::cerr << "  Gojo - Zwave Based house automation software  (gojo.birukus.com)  " << std::endl;
      std::cerr << "SYNOPSIS  " << std::endl;
      std::cerr << "  terminal> gojo /path/config_file.json  " << std::endl;
      std::cerr << "DESCRIPTION  " << std::endl;
      std::cerr << "  config_file: json file describing running parameters for Gojo  " << std::endl<< std::endl;


      return 1;
    }

  // input config file arguments
  const std::string configFilename = argv[1];


  // start the controller
  Gojo::Controller* cont = new Gojo::Controller(configFilename);

  // run the program
  return cont->run();
}
