///*!
//*
//*   Birukus ( www.birukus.com )
//*
//*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
//*
//*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
//*
//*   Date: 09/2015
//*
//*/

//#ifndef ZWAVE_H
//#define ZWAVE_H

//#include "../../util/singleton.h"
//#include "../../util/settings.h"
//#include "../../util/logger.h"
//#include "../../device/devicecontroller.h"
//#include "../../device/device.h"
//#include "../../device/nodes/nodeDatatypes.h"
//#include "../driver.h"

//#include <unistd.h>
//#include <stdlib.h>
//#include <stdio.h>
//#include <pthread.h>
//#include <time.h>
//#include <list>
//#include <QList>

//#include "openzwave/Options.h"
//#include "openzwave/Manager.h"
//#include "openzwave/Driver.h"
//#include "openzwave/Node.h"
//#include "openzwave/Group.h"
//#include "openzwave/Notification.h"
//#include "openzwave/value_classes/ValueStore.h"
//#include "openzwave/value_classes/Value.h"
//#include "openzwave/value_classes/ValueBool.h"
//#include "openzwave/platform/Log.h"


//namespace Gojo{

//    namespace Zwave{

//        struct NodeData
//        {
//            bool               isInit =  false;
//            const OpenZWave::ValueID* valueID;
//            uint8              commandClassID;
//            uint8              nodeType;
//            std::string        valueLabel;
//            std::string        valueUnits;
//            std::string        valueHelp;
//            int                valueMin;
//            int                valueMax;
//            std::string        valueStr;
//            int                valueInt;
//            bool               valueBool;
//            float              valueFloat;
//        public:
//            NodeData(){}
//        };

//        typedef struct
//        {
//            uint32		m_homeId;
//            uint8		m_nodeId;
//            bool		m_polled;
//            std::list<NodeData>	m_values;
//        }NodeInfo;

//        /**
//         * @brief The ZwaveController class
//         * This class controlls the incoming/outgoing signals from the USB hub
//         */
//        class ZwaveDriver: public Gojo::Drivers::Driver, public Gojo::Util::Singleton<ZwaveDriver>
//        {
//            friend class Gojo::Util::Singleton<ZwaveDriver>;


//        private: // Z-wave specific

//            // which USB Port to use
//            std::string mUSBPort;

//            // xml device config path
//            std::string mXMLConfigPath;

//            // home id
//            uint32 m_homeId; // TODO change

//            // init status
//            bool   m_initFailed = false;

//            std::list<NodeInfo*> m_nodes;

//            pthread_mutex_t m_procMutex;
//            // mutex used in init
//            pthread_cond_t  m_initCond  = PTHREAD_COND_INITIALIZER;
//            pthread_mutex_t m_initMutex = PTHREAD_MUTEX_INITIALIZER;


//        private: // variables

//            bool isInitialized = false;

//            QList<Gojo::Devices::Device> mDeviceList;

//        private: // methods

//            ZwaveDriver();

////            void initZWave(Gojo::Devices::DeviceController* aController=nullptr);

//            void initZWave();

//            void shutdownZWave();

//            void initPThreads();

//            void lockThread();

//            void unlockThread();

//            NodeInfo* GetNodeInfo(OpenZWave::Notification const* _notification);

//        public: // public methods

//            ~ZwaveDriver();

//            void init();

////            bool addDevice(Gojo::Devices::Device aDevice){ return true;}

//            bool outputNodeSignal(Gojo::Devices::NodeOutputData aNode ){ return true; }

//            int testmain( int argc, char* argv[] );

//            int test2( int argc, char* argv[] );

//            static void ZwaveControllerzwaveManagerCallBack( OpenZWave::Notification const* _notification, void* _context );

//            /**
//             * @brief Gojo::Zwave::ZwaveController::setDeviceValue
//             * @param nodeid - device id
//             * @param value  - device value
//             * @return  status, whether the change was successful
//             */
//            bool setDeviceValue(int nodeid, bool value);

//            /**
//             * @brief Gojo::Zwave::ZwaveController::getDeviceValue
//             * @param nodeid  - device id
//             * @return   value of the device id
//             */
//            bool getDeviceValue(int nodeid);


//        };
//    }
//} //#namespace GOJO
//#endif // ZWAVE_H

