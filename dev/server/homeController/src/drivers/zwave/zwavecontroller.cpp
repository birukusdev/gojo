///*!
//*
//*   Birukus ( www.birukus.com )
//*
//*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
//*
//*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
//*
//*   Date: 09/2015
//*
//*/

//#include "zwavedriver.h"

//Gojo::Zwave::ZwaveDriver::ZwaveDriver()
//{


//}

//Gojo::Zwave::ZwaveDriver::~ZwaveDriver()
//{


//}

///**
// * @brief Gojo::Zwave::ZwaveController::init
// *
// * initialize the controller
// */
//void Gojo::Zwave::ZwaveDriver::init()
//{

//    Gojo::Util::Logger* log = Gojo::Util::Logger::getInstance();

//    // if this has already been initialized, stop!
//    if(isInitialized)
//        return;

//    log->log("Gojo::Zwave::ZwaveController::init(): initializing zwave");

//    // get parameters from the logger
//    // get the usb port
//    Gojo::Util::Settings::ptr()->get("ZWave-USB-Port",mUSBPort);

//    // get the device xml config file
//    Gojo::Util::Settings::ptr()->get("ZWave-Device-XML-Config-Path",mXMLConfigPath);

//    ////////////////
//    ///  Initialize threads
//    ////////////////
//    initPThreads();

//    log->log("Gojo::Zwave::ZwaveController::init(): pthread initialized");

//    ////////////////
//    ///  Initialize Zwave
//    ////////////////
//    initZWave();

//    log->log("Gojo::Zwave::ZwaveController::init(): zwave config initialized");


//    // wait until the z-wave system initializes
//    pthread_mutex_lock(&m_initMutex);

//    log->log("Gojo::Zwave::ZwaveController::init(): zwave config ... waiting for Manager init feedback...");
//    // wait until zwave-manager is ready
//    // the Manager call back function will change the value in m_initcond
//    pthread_cond_wait( &m_initCond, &m_initMutex );

//    // unlock the mutex and continue
//    pthread_mutex_unlock(&m_initMutex);

//    isInitialized = true;

//    log->log("Gojo::Zwave::ZwaveController::init(): zwave config complete");
//}

///**
// * @brief Gojo::Zwave::ZwaveController::initZWave
// */
////void Gojo::Zwave::ZwaveController::initZWave(Gojo::Devices::DeviceController* aController)
//void Gojo::Zwave::ZwaveDriver::initZWave()
//{
////    controller = aController;

//    std::string xmlConfigFilePath;

//    Gojo::Util::Settings::ptr()->get("ZWave-Device-XML-Config-Path",xmlConfigFilePath);

//    OpenZWave::Options::Create(xmlConfigFilePath.c_str(), "", "" );
//    OpenZWave::Options::Get()->AddOptionInt( "SaveLogLevel", OpenZWave::LogLevel_Detail );
//    OpenZWave::Options::Get()->AddOptionInt( "QueueLogLevel", OpenZWave::LogLevel_Debug );
//    OpenZWave::Options::Get()->AddOptionInt( "DumpTrigger", OpenZWave::LogLevel_Error );
//    OpenZWave::Options::Get()->AddOptionInt( "PollInterval", 500 );

//    // Comment the following line out if you want console logging
//    OpenZWave::Options::Get()->AddOptionBool( "ConsoleOutput", false );

//    OpenZWave::Options::Get()->AddOptionBool( "IntervalBetweenPolls", true );
//    OpenZWave::Options::Get()->AddOptionBool("ValidateValueChanges", true);
//    OpenZWave::Options::Get()->Lock();

//    //
//    // Configure the Manager to handle callbacks
//    //

//    OpenZWave::Manager::Create();
//    // Zwave manager call back function
//    OpenZWave::Manager::Get()->AddWatcher( Gojo::Zwave::ZwaveDriver::ZwaveControllerzwaveManagerCallBack, NULL );
//    OpenZWave::Manager::Get()->AddDriver( mUSBPort );

//}



///**
// * @brief Gojo::Zwave::ZwaveController::shutdownZWave
// */
//void Gojo::Zwave::ZwaveDriver::shutdownZWave()
//{
//    // remove the usb driver
//    if( strcasecmp( mUSBPort.c_str(), "usb" ) == 0 )
//        OpenZWave::Manager::Get()->RemoveDriver( "HID Controller" );
//    else
//        OpenZWave::Manager::Get()->RemoveDriver( mUSBPort );

//    // remove the call back function
//    OpenZWave::Manager::Get()->RemoveWatcher( Gojo::Zwave::ZwaveDriver::ZwaveControllerzwaveManagerCallBack, NULL );
//    OpenZWave::Manager::Destroy();
//    OpenZWave::Options::Destroy();

//    // destroy mutex
//    pthread_mutex_destroy( &m_procMutex ); //m_procMutex
//    pthread_mutex_destroy( &m_initMutex );
//}
///**
// * @brief Gojo::Zwave::ZwaveController::initPThreads
// */
//void Gojo::Zwave::ZwaveDriver::initPThreads()
//{
//    // add -pthread to gcc flags

//    // pthread - mutex attr
//    pthread_mutexattr_t mutexattr; // mutex attribute for starting threads

//    // configure m_procMutex

//    // Set up mutual exclusion so that this thread has priority
//     pthread_mutexattr_init ( &mutexattr ); // init the mutex attribute

//     // PTHREAD_MUTEX_RECURSIVE sets the mutex so that only one instance can exist
//     pthread_mutexattr_settype( &mutexattr, PTHREAD_MUTEX_RECURSIVE ); // set the attribute

//     pthread_mutex_init( &m_procMutex, &mutexattr ); // init the mutex init

//     pthread_mutexattr_destroy( &mutexattr ); // destrow the attribute

//}

////-----------------------------------------------------------------------------
//// <GetNodeInfo>
//// Return the NodeInfo object associated with this notification
////---------------------------------------------------------------------------
//Gojo::Zwave::NodeInfo* Gojo::Zwave::ZwaveDriver::GetNodeInfo(const OpenZWave::Notification* _notification)
//{
//    const uint32 homeId = _notification->GetHomeId();
//    const uint8  nodeId = _notification->GetNodeId();

//    // use an iterator to go through the list of detected nodes
//    for( std::list<NodeInfo*>::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it )
//    {
//        NodeInfo* nodeInfo = *it;
//        if( ( nodeInfo->m_homeId == homeId ) && ( nodeInfo->m_nodeId == nodeId ) )
//        {
//            return nodeInfo;
//        }
//    }

//    return NULL;
//}


///**
// * @brief Gojo::Zwave::ZwaveControllerzwaveManagerCallBack
// * @param _notification
// * @param _context
// *
// * This function was made static to allow the zwave manager control
// *
// */
//void Gojo::Zwave::ZwaveDriver::ZwaveControllerzwaveManagerCallBack( OpenZWave::Notification const* _notification, void* _context )
//{

////    printf(" --Callback ---- : homeid=%d nodeid=%d notification=%d event=%d  value=%d\n",
////           _notification->GetHomeId(),_notification->GetNodeId(),_notification->GetNotification(),_notification->GetEvent(),_notification->GetValueID());

//    printf("----callback---- Type=%d \n",_notification->GetType());

//    Gojo::Zwave::ZwaveDriver* cont = Gojo::Zwave::ZwaveDriver::getInstance();

//    // Must do this inside a critical section to avoid conflicts with the main thread
//    pthread_mutex_lock( &cont->m_procMutex );

//    switch( _notification->GetType() )
//    {
//        // New-Node added to the list
//        case OpenZWave::Notification::Type_NodeAdded:
//        {
//            // Add the new node to our list

//            // create new nodeInfo
//            NodeInfo* nodeInfo = new NodeInfo();
//            nodeInfo->m_homeId = _notification->GetHomeId();
//            nodeInfo->m_nodeId = _notification->GetNodeId();
//            nodeInfo->m_polled = false; // TODO: figure this out

//            // add the node to the list
//            cont->m_nodes.push_back( nodeInfo );

//            //    printf(" ------------- Node got added : homeid=%d nodeid=%d notification=%d event=%d  value=%d\n",
//            //           _notification->GetHomeId(),_notification->GetNodeId(),_notification->GetNotification(),_notification->GetEvent(),_notification->GetValueID());

//            printf(" ------------- Node got added : homeid=%u nodeid=%u notification=%d event=%d  value=%d\n",
//                   nodeInfo->m_homeId,nodeInfo->m_nodeId,0,0,0);

//            Gojo::Util::Logger::getInstance()->log(QString("Gojo::Zwave::ZwaveController::ZwaveControllerzwaveManagerCallBack(): Node got added to the list nodeid=")+nodeInfo->m_homeId+" homeid:"+ nodeInfo->m_homeId );

//            break;
//        }

//        // Remove a node from the list
//        case OpenZWave::Notification::Type_NodeRemoved:
//        {
//            // Remove the node from our list
//            uint32 const homeId = _notification->GetHomeId();
//            uint8 const nodeId = _notification->GetNodeId();

//            // search through the list of nodes
//            for( std::list<NodeInfo*>::iterator it = cont->m_nodes.begin(); it != cont->m_nodes.end(); ++it )
//            {
//                NodeInfo* nodeInfo = *it;

//                if( ( nodeInfo->m_homeId == homeId ) && ( nodeInfo->m_nodeId == nodeId ) )
//                {
//                    cont->m_nodes.erase( it );
//                    delete nodeInfo;
//                    break;
//                }
//            }
//            break;
//        }

//        // A Device has added a value type
//        case OpenZWave::Notification::Type_ValueAdded:
//        {
//            if( Gojo::Zwave::NodeInfo* nodeInfo = cont->GetNodeInfo( _notification ) )
//            {
//                Gojo::Zwave::NodeData* nodeValueData = new Gojo::Zwave::NodeData();

//                nodeValueData->valueID    = &_notification->GetValueID(); // TODO: make this a copy
//                nodeValueData->valueLabel = OpenZWave::Manager::Get()->GetValueLabel(*nodeValueData->valueID);
//                nodeValueData->valueHelp  = OpenZWave::Manager::Get()->GetValueHelp(*nodeValueData->valueID);
//                nodeValueData->valueUnits = OpenZWave::Manager::Get()->GetValueUnits(*nodeValueData->valueID);
//                nodeValueData->valueMin   = OpenZWave::Manager::Get()->GetValueMin(*nodeValueData->valueID);
//                nodeValueData->valueMax   = OpenZWave::Manager::Get()->GetValueMax(*nodeValueData->valueID);
//                nodeValueData->commandClassID = nodeValueData->valueID->GetCommandClassId();
//                nodeValueData->nodeType   = nodeValueData->valueID->GetType();
//                nodeValueData->isInit = true;

//                OpenZWave::Manager::Get()->GetValueAsBool(*nodeValueData->valueID,&nodeValueData->valueBool);
//                OpenZWave::Manager::Get()->GetValueAsInt(*nodeValueData->valueID,&nodeValueData->valueInt);
//                OpenZWave::Manager::Get()->GetValueAsString(*nodeValueData->valueID,&nodeValueData->valueStr);
//                OpenZWave::Manager::Get()->GetValueAsFloat(*nodeValueData->valueID,&nodeValueData->valueFloat);

//                // Add the new value to our list
//                nodeInfo->m_values.push_back( *nodeValueData );


////                printf(" ------------- Value added : homeid=%u nodeid=%u notification=%d valueID=%d  valueStr=%s\n",
////                       nodeInfo->m_homeId,nodeInfo->m_nodeId,0,nodeValueData->valueID,nodeValueData->valueStr.c_str());

//                printf(" ------------- Value added : homeid=%u nodeid=%u classid=%d, type=%d, valueID=%d  valueStr=%s  valueLabel=%s valueHelp=%s valueUnits=%s\n",
//                       nodeInfo->m_homeId,nodeInfo->m_nodeId,nodeValueData->valueID->GetCommandClassId(),nodeValueData->nodeType,nodeValueData->valueID,nodeValueData->valueStr.c_str(),
//                       nodeValueData->valueLabel.c_str(),nodeValueData->valueHelp.c_str(),nodeValueData->valueUnits.c_str());
//                break;
//            }
//            break;


//        }

//        // remove the node value
//        case OpenZWave::Notification::Type_ValueRemoved:
//        {
//            // get the right node, if it exists in the list of nodes added
//            if( NodeInfo* nodeInfo = cont->GetNodeInfo( _notification ) )
//            {
//                // Remove the value of the node from the list
//                for( std::list<NodeData>::iterator it = nodeInfo->m_values.begin(); it != nodeInfo->m_values.end(); ++it )
//                {
//                    // check if the current node value matches
//                    if( (*it->valueID) == _notification->GetValueID() )
//                    {
//                        // remove this node
//                        nodeInfo->m_values.erase( it );
//                        break;
//                    }
//                }
//            }
//            break;
//        }

//        // add a value change to the node
//        case OpenZWave::Notification::Type_ValueChanged:
//        {
//            // One of the node values has changed
//            if( NodeInfo* nodeInfo = cont->GetNodeInfo( _notification ) )
//            {
//                // search through the value of the node from the list
//                for( std::list<NodeData>::iterator it = nodeInfo->m_values.begin(); it != nodeInfo->m_values.end(); ++it )
//                {

//                   // check if the current node value matches
//                    if( (*it).commandClassID == _notification->GetValueID().GetCommandClassId() )
//                    {
//                        NodeData* n = &(*it);

//                        OpenZWave::Manager::Get()->GetValueAsBool(*it->valueID,&it->valueBool );
//                        OpenZWave::Manager::Get()->GetValueAsInt(*it->valueID,&it->valueInt);
//                        OpenZWave::Manager::Get()->GetValueAsString(*it->valueID,&it->valueStr);
//                        OpenZWave::Manager::Get()->GetValueAsFloat(*it->valueID,&it->valueFloat);

//                        printf(" ------------- Value Changed : homeid=%u nodeid=%u classid=%d type=%d, valueStr=%s,  valueLabel=%s, valueHelp=%s, valueUnits=%s\n",
//                               nodeInfo->m_homeId,nodeInfo->m_nodeId,n->commandClassID,
//                               n->nodeType,n->valueStr.c_str(),n->valueLabel.c_str(),
//                               n->valueHelp.c_str(),n->valueUnits.c_str());
//                        break;
//                    }
//                }
//            }
//            break;
//        }

//        case OpenZWave::Notification::Type_Group:
//        {
//            // One of the node's association groups has changed
//            if( NodeInfo* nodeInfo = cont->GetNodeInfo( _notification ) )
//            {
//                nodeInfo = nodeInfo;		// placeholder for real action
//            }
//            break;
//        }



//        case OpenZWave::Notification::Type_NodeEvent:
//        {
//            // We have received an event from the node, caused by a
//            // basic_set or hail message.
//            if( NodeInfo* nodeInfo = cont->GetNodeInfo( _notification ) )
//            {
//                nodeInfo = nodeInfo;		// placeholder for real action
//            }
//            break;
//        }

//        case OpenZWave::Notification::Type_PollingDisabled:
//        {
//            if( NodeInfo* nodeInfo = cont->GetNodeInfo( _notification ) )
//            {
//                nodeInfo->m_polled = false;
//            }
//            break;
//        }

//        case OpenZWave::Notification::Type_PollingEnabled:
//        {
//            if( NodeInfo* nodeInfo = cont->GetNodeInfo( _notification ) )
//            {
//                nodeInfo->m_polled = true;
//            }
//            break;
//        }

//        case OpenZWave::Notification::Type_DriverReady:
//        {
//            cont->m_homeId = _notification->GetHomeId();
//            break;
//        }

//        case OpenZWave::Notification::Type_DriverFailed:
//        {
//            cont->m_initFailed = true;
//            pthread_cond_broadcast(&cont->m_initCond);

//            Gojo::Util::Logger::getInstance()->error("Gojo::Zwave::ZwaveController::ZwaveControllerzwaveManagerCallBack(): OpenZwave drive init failed");
//            break;
//        }

//        case OpenZWave::Notification::Type_AwakeNodesQueried:
//        case OpenZWave::Notification::Type_AllNodesQueried:
//        case OpenZWave::Notification::Type_AllNodesQueriedSomeDead:
//        {
//            // this will send a signal for the rest of the program to continue
//            pthread_cond_broadcast(&cont->m_initCond);
//            break;
//        }

//        case OpenZWave::Notification::Type_DriverReset:
//        case OpenZWave::Notification::Type_Notification:
//        case OpenZWave::Notification::Type_NodeNaming:
//        case OpenZWave::Notification::Type_NodeProtocolInfo:
//        case OpenZWave::Notification::Type_NodeQueriesComplete:
//        default:
//        {
//        }
//    }

//    pthread_mutex_unlock( &cont->m_procMutex );
//}

///**
// * @brief Gojo::Zwave::ZwaveController::setDeviceValue
// * @param nodeid - device id
// * @param value  - device value
// * @return  status, whether the change was successful
// */
//bool Gojo::Zwave::ZwaveDriver::setDeviceValue(int nodeid, bool value)
//{

//    pthread_mutex_lock( &m_procMutex ); // obtain lock before processing

//    // go through all the nodes
//    for( std::list<NodeInfo*>::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it )
//    {
//        NodeInfo* nodeInfo = *it;

//        // check if the node id is correct
//        if( nodeInfo->m_nodeId != nodeid )
//            continue;

////        // for the correct node id
////        for( std::list<OpenZWave::ValueID>::iterator it2 = nodeInfo->m_values.begin(); it2 != nodeInfo->m_values.end(); ++it2 )
////        {
////            // pointer to the value
////            OpenZWave::ValueID v = *it2;

////            // ? what is a class id?
////            if( v.GetCommandClassId() == 0x25)
////            {
////                bool* status;


////                printf("\n Setting Node %d to %s ",nodeInfo->m_nodeId,value ? "On" : "Off");
////                // set the ValueID with the right classID
////               OpenZWave::Manager::Get()->SetValue(v, value);

////                printf("\n Node %d is now %s \n",nodeInfo->m_nodeId,OpenZWave::Manager::Get()->GetValueAsBool(v, status) ? "ON" : "OFF");

////                break;
////            }
////        }
//    }

//    pthread_mutex_unlock( &m_procMutex );
//}

///**
// * @brief Gojo::Zwave::ZwaveController::getDeviceValue
// * @param nodeid  - device id
// * @return   value of the device id
// */
//bool Gojo::Zwave::ZwaveDriver::getDeviceValue(int nodeid)
//{
//    pthread_mutex_lock( &m_procMutex ); // obtain lock before processing

//    // go through all the nodes
//    for( std::list<NodeInfo*>::iterator it = m_nodes.begin(); it != m_nodes.end(); ++it )
//    {
//        NodeInfo* nodeInfo = *it;

//        // check if the node id is correct
//        if( nodeInfo->m_nodeId != nodeid )
//            continue;

////        // for the correct node id
////        for( std::list<OpenZWave::ValueID>::iterator it2 = nodeInfo->m_values.begin(); it2 != nodeInfo->m_values.end(); ++it2 )
////        {
////            // pointer to the value
////            OpenZWave::ValueID v = *it2;

////            std::string typestr = OpenZWave::Manager::Get()->GetNodeDeviceTypeString(nodeInfo->m_homeId,nodeInfo->m_nodeId);


////            std::string valstr;
////            OpenZWave::Manager::Get()->GetValueAsString(v,&valstr);

////            printf("---homeid=%u , nodeid=%u type=%s  value=%s classID=%X \n ",nodeInfo->m_homeId,nodeInfo->m_nodeId,typestr.c_str(),valstr.c_str(),v.GetCommandClassId());

////        }
//    }

//    pthread_mutex_unlock( &m_procMutex );
//}


