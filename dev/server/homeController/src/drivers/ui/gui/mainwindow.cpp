/*!
*
*   Birukus ( www.birukus.com )
*
*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
*
*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
*
*   Date: 09/2015
*
*/

#include "mainwindow.h"


Gojo::Ui::MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Gojo::Ui::MainWindow)
{
//    ui->setupUi(this);
}

Gojo::Ui::MainWindow::~MainWindow()
{
    delete ui;
}
