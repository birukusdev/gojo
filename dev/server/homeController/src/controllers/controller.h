#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "../util/settings.h"
#include "../util/logger.h"
#include "../util/tools.h"
#include "../device/device.h"

namespace Gojo
{
/**
 * @brief The Controller class
 *
 * This is the main controller for the Gojo program
 *
 * The purpose of this class is to run the entire program
 */
class Controller
{
private: // variables
    QList<Gojo::Devices::Device*> mDeviceList;

private: // methods

    /**
     * @brief init - initialize all the drivers and start the processing routines
     * @return
     */
    bool init();

    /**
     * @brief initDrivers
     * @return
     */
    bool initDrivers();

   /**
    * @brief initDevices
    * @return
    */
    bool initDevices();

public:
    /**
     * @brief Controller
     *
     * main gojo initialization
     */
    Controller(const std::string aConfigFilename);

    ~Controller();

    /**
     * @brief run
     * @return
     */
    bool run();
};


}

#endif // CONTROLLER_H
