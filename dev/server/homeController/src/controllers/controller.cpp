#include "controller.h"

using namespace Gojo;

// main constructor
Controller::Controller(const std::string aConfigFilename)
{
    // load config file
    Gojo::Util::Settings::getInstance()->init(aConfigFilename);

    // start the logger
    Gojo::Util::Logger* log = Gojo::Util::Logger::getInstance();

    log->init(); // start the logger

    // check to see if the logger and settings have been started
    if(!Gojo::Util::Settings::getInstance()->isRunning() || !Gojo::Util::Logger::getInstance()->isRunning())
        Gojo::Util::Tools::throwError(" Gojo Controller started without Logger and Settings being loaded");

    // initialize the controller
    init();
}

// main seconstructor
Controller::~Controller()
{
    for( int n=0; n<mDeviceList.size(); n++)
        delete mDeviceList.at(n);
}

bool Controller::init()
{
    // init devices
    initDevices();

    // init drivers

    // init event processors

    // init


}

bool Controller::initDrivers()
{
    //
return true;
}

bool Controller::initDevices()
{
    // load the directory where all the device files are stored
    QString deviceDirPath = Gojo::Util::Settings::getInstance()->get("Device-Config-Directory");

    // get a list of device files in the directory
    QList<QString> deviceFilenames = Gojo::Util::Tools::getFilesInDirectory(deviceDirPath,"devices.xml");

    // go through all the device files and load every device
    for(QString file : deviceFilenames)
        mDeviceList.append(new Gojo::Devices::Device(file));

    //

    //

    //
    return true;
}

bool Controller::run()
{

    return true;
}
