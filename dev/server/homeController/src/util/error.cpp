/*!
*
*   Birukus ( www.birukus.com )
*
*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
*
*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
*
*   Date: 09/2015
*
*/


#include "error.h"


void Gojo::Util::Error::initSystemErrorHandle()
{

  // register signal SIGINT and signal handler
  Gojo::Util::act.sa_sigaction = signalHandler;
  Gojo::Util::act.sa_flags = SA_SIGINFO;
  sigaction(SIGTERM, &Gojo::Util::act, NULL);
  sigaction(SIGKILL, &Gojo::Util::act, NULL);
  sigaction(SIGSEGV, &Gojo::Util::act, NULL);
  sigaction(SIGHUP, &Gojo::Util::act, NULL);
  sigaction(SIGINT, &Gojo::Util::act, NULL);
  sigaction(SIGQUIT, &Gojo::Util::act, NULL);
  sigaction(SIGILL, &Gojo::Util::act, NULL);
  sigaction(SIGXFSZ, &Gojo::Util::act, NULL);

}


/**
 * This function captures the signals from the running environment
 *
 * @param signum
 * @param info
 * @param ptr
 */
void Gojo::Util::Error::signalHandler(int signum, siginfo_t *info, void *ptr) //  , std::string aMessage)
{
   // get signal number
   // signum

   // get origin of signal
   //(unsigned long)info->si_pid,info->si_errno);

    if(signum==9)
        handleSystemError(USER_KILL);
    else if(signum==11)
        handleSystemError(MEMORY_ERROR);
    else
        handleSystemError(UNKNOWN_ERROR);

    //#define SIGABRT     6  when memory is too small
    std::exit(signum);
}

/**
 * @brief Gojo::Util::Error::handleSystemError
 * @param err
 *
 * handles the app response to the error
 */
void Gojo::Util::Error::handleSystemError(ErrorTypes err)
{



}
