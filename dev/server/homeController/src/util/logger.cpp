/*!
*
*   Birukus ( www.birukus.com )
*
*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
*
*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
*
*   Date: 09/2015
*
*/

#include "logger.h"
#include "settings.h"
#include "error.h"
#include "tools.h"

using namespace Gojo::Util;

Gojo::Util::Logger::Logger()
{

}
Gojo::Util::Logger::~Logger()
{

}
void Gojo::Util::Logger::init()
{
    if(!Settings::getInstance()->isRunning())
        // the Settings has not been started
        Tools::throwError("Logger.init() - started without first starting Settings()");

    QString logFilename;

    Settings::ptr()->get("Log-File-Path",logFilename);

    qDebug()<<"Logger: opening filename for writing logs"<<logFilename;

    isStarted = true;
}
