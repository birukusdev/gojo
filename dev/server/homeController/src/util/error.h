/*!
*
*   Birukus ( www.birukus.com )
*
*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
*
*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
*
*   Date: 09/2015
*
*/

#ifndef ERROR_H
#define ERROR_H

#include <signal.h>  // sa_sigaction
#include <iostream> // cout
#include <cstdio>   // printf


namespace Gojo{

  namespace Util{

    enum ErrorTypes{UNKNOWN_ERROR,MEMORY_ERROR,USER_KILL,SYSTEM_KILL};

    static struct sigaction act; // signals from the system

    /**
     * @brief The Error class
     *
     * This class handles system errors from the unix system signals
     * It also handles output errors from the program
     */
    class Error
    {
    private:
        // constructor has been made private for now
        Error(){} // private constructor
        ~Error(){}

    public:

      /**
       * @brief initSystemErrorHandle
       * initializes the call back for system error events
       */
      static void initSystemErrorHandle();


      /**
       * Handling signals from system
       * @param signum
       * @param info
       * @param ptr
       */
      static void signalHandler(int signum, siginfo_t *info, void *ptr);

      /**
       * @brief handleSystemError
       * assigns a function to respond to the system error signals
       */
      static void handleSystemError(ErrorTypes err );

    };

  }
}
#endif // ERROR_H

//
///* Signals.  */
//#define SIGHUP      1   /* Hangup (POSIX).  */
//#define SIGINT      2   /* Interrupt (ANSI).  */
//#define SIGQUIT     3   /* Quit (POSIX).  */
//#define SIGILL      4   /* Illegal instruction (ANSI).  */
//#define SIGTRAP     5   /* Trace trap (POSIX).  */
//#define SIGABRT     6   /* Abort (ANSI).  */
//#define SIGIOT      6   /* IOT trap (4.2 BSD).  */
//#define SIGBUS      7   /* BUS error (4.2 BSD).  */
//#define SIGFPE      8   /* Floating-point exception (ANSI).  */
//#define SIGKILL     9   /* Kill, unblockable (POSIX).  */
//#define SIGUSR1     10  /* User-defined signal 1 (POSIX).  */
//#define SIGSEGV     11  /* Segmentation violation (ANSI).  */
//#define SIGUSR2     12  /* User-defined signal 2 (POSIX).  */
//#define SIGPIPE     13  /* Broken pipe (POSIX).  */
//#define SIGALRM     14  /* Alarm clock (POSIX).  */
//#define SIGTERM     15  /* Termination (ANSI).  */
//#define SIGSTKFLT   16  /* Stack fault.  */
//#define SIGCLD      SIGCHLD /* Same as SIGCHLD (System V).  */
//#define SIGCHLD     17  /* Child status has changed (POSIX).  */
//#define SIGCONT     18  /* Continue (POSIX).  */
//#define SIGSTOP     19  /* Stop, unblockable (POSIX).  */
//#define SIGTSTP     20  /* Keyboard stop (POSIX).  */
//#define SIGTTIN     21  /* Background read from tty (POSIX).  */
//#define SIGTTOU     22  /* Background write to tty (POSIX).  */
//#define SIGURG      23  /* Urgent condition on socket (4.2 BSD).  */
//#define SIGXCPU     24  /* CPU limit exceeded (4.2 BSD).  */
//#define SIGXFSZ     25  /* File size limit exceeded (4.2 BSD).  */
//#define SIGVTALRM   26  /* Virtual alarm clock (4.2 BSD).  */
//#define SIGPROF     27  /* Profiling alarm clock (4.2 BSD).  */
//#define SIGWINCH    28  /* Window size change (4.3 BSD, Sun).  */
//#define SIGPOLL     SIGIO   /* Pollable event occurred (System V).  */
//#define SIGIO       29  /* I/O now possible (4.2 BSD).  */
//#define SIGPWR      30  /* Power failure restart (System V).  */
//#define SIGSYS      31  /* Bad system call.  */
//#define SIGUNUSED   31
