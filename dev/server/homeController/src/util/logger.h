/*!
*
*   Birukus ( www.birukus.com )
*
*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
*
*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
*
*   Date: 09/2015
*
*/

#ifndef LOGGER
#define LOGGER

#include <QString>
#include <string>
#include <QDebug>
#include <QMap>
#include <QFile>

#include "singleton.h"

namespace Gojo{

namespace Util{

    class Logger: public Singleton<Logger>
    {
            friend class Singleton<Logger>;

    private:
        // some private constructor

        Logger();

        bool consolePrintLogMessages = false;

        bool isStarted = false;

    public:

        ~Logger();

        // initialize the logger and start writing header material

        void init();

        // methods for writing logs

        void warn(QString msg){ qDebug()<<"Warning: "<<msg;}

        void error(QString msg){qDebug()<<"Error  : "<<msg;}

        void log(QString msg){  qDebug()<<"Log    : "<<msg;}

        bool isRunning(){ return isStarted;}
    };
}
}
#endif // LOGGER


