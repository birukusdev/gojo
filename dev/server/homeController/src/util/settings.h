/*!
*
*   Birukus ( www.birukus.com )
*
*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
*
*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
*
*   Date: 09/2015
*
*/

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QString>
#include <string>
#include <QDebug>
#include <QMap>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QVector>

// Gojo includes
#include "tools.h"
#include "singleton.h"

namespace Gojo{

  namespace Util{

      class Settings: public Singleton<Settings>
      {
          friend class Singleton<Settings>;

      private:

          bool mSettingsLoaded = false;

          QString mFilename;

          QMap<QString,double> mIntMap;
          QMap<QString,QString> mStrMap;
          QMap<QString,QList<QString>> mArrMap;

          /**
           * @brief throwNoAttributesError
           * @param key
           *
           * This function is used to throw a more meaningful error
           */
          void throwAttributeError(QString key)
          {
            Tools::throwError(QString(" Settings: unable to retreive parameter named ")+key);
          }

      private:
          // constructor
          Settings();

          // methods
          // read json file
          void readJson();
      public:
          void init(const std::string aFilename);

          bool isRunning(){ return mSettingsLoaded; }

          bool get(QString key,QString &ret){
              if(mStrMap.contains(key))
              {
                  ret = mStrMap.value(key);
                  return true;
              }
              else
                  throwAttributeError(key);

              return false;
          }

          bool get(char* key,std::string &ret){
              if(mStrMap.contains(QString(key)))
              {
                  ret = mStrMap.value(QString(key)).toStdString();
                  return true;
              }
              else
                  throwAttributeError(QString(key));

              return false;
          }

          bool get(QString key,double &ret){
              if(mIntMap.contains(key))
              {
                  ret = mIntMap.value(key);
                  return true;
              }
              else
                  throwAttributeError(key);

              return false;
          }

//          bool get(QString key,QList<double> &ret){
//              if(mArrMap.contains(key))
//              {
//                  ret = mIntMap.value(key);
//                  return true;
//              }
//              else
//                  throwAttributeError(key);

//              return false;
//          }

          ////////////////////////////////

          QString get(QString key)
          {
              if(mStrMap.contains(key))
                  return mStrMap.value(key);
              else
                  throwAttributeError(key);

              return QString("");
          }

          std::string get(char* key)
          {
              if(mStrMap.contains(QString(key)))
                  return mStrMap.value(QString(key)).toStdString();
              else
                  throwAttributeError(QString(key));

              return "";
          }

//          double get(QString key){
//              if(mIntMap.contains(key))
//                  return mIntMap.value(key);
//              else
//                  throwAttributeError(key);

//              return 0;
//          }

//          QList<QString> get(QString key){
//              if(mArrMap.contains(key))
//                  return mArrMap.value(key);
//              else
//                  throwAttributeError(key);

//              return QList<QString>();
//          }
      };
  }
}
#endif // SETTINGS_H
