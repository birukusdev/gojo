/*!
*
*   Birukus ( www.birukus.com )
*
*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
*
*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
*
*   Date: 09/2015
*
*/
#ifndef TOOLS_H
#define TOOLS_H

#include <QtGlobal>
#include <QString>
#include <QFileInfo>
#include <QList>

namespace Gojo{
namespace Util{
  /**
   * @brief The Tools class
   *
   * contains methods used by multiple classes
   */
  class Tools
  {

  private:
    Tools(){}

  public:

    /**
     * @brief checkFileExists
     * @param aFilePath
     *
     * Checks if the given file exists, otherwise, throws error
     */
    static void checkFileExists(QString aFilePath);

    /**
     * @brief throwError
     * @param aMsg
     *
     * basic method for throwing errors
     */
    static void throwError(QString aMsg);

    /**
     * does what printf does but returns a string instead
     *
     * it also uses % to represent, numbers and strings
     *
     * @param str
     * @param list
     * @return
     */
    static std::string prints(std::string str, std::initializer_list<double> list);


    static QList<QString> getFilesInDirectory(const QString aDirPath,const QString aExtension=".xml");
  };
}

}

#endif // TOOLS_H
