/*!
*
*   Birukus ( www.birukus.com )
*
*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
*
*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
*
*   Date: 09/2015
*
*/

#include "settings.h"

Gojo::Util::Settings::Settings()
{

}

/**
 * @brief Gojo::Util::Settings::init
 * @param aFilename
 *
 * init
 * open the json file
 */
void Gojo::Util::Settings::init(const std::string aFilename)
{
  mFilename = QString(aFilename.c_str());

  // check if file exists, otherwise, throw error
  Gojo::Util::Tools::checkFileExists(mFilename);

  readJson();

  mSettingsLoaded = true;
}


/**
 * @brief Gojo::Util::Settings::readJson
 * read the json file
 */
void Gojo::Util::Settings::readJson()
{
  QString val;

  QFile file;
  file.setFileName(mFilename);
  file.open(QIODevice::ReadOnly | QIODevice::Text);
  val = file.readAll(); // read the whole file
  file.close(); // close file

  // read the whole document
  QJsonDocument jsonDoc = QJsonDocument::fromJson(val.toUtf8());
  // convert the document into a json obj
  QJsonObject jsonObj = jsonDoc.object();

  // get variable
  //QJsonValue param = jsonObj.value("name")

  // get subobject
  // QJsonValue subParam = param["name"]

  // get array
  // QJsonArray arr = param[n];

  QStringList attList = jsonObj.keys();

  if(attList.size()<1)
    Gojo::Util::Tools::throwError("Settings::readJson()  JSON file formatting ERROR, unable to read json file correctly");

  foreach( QString key, attList)
  {
      QJsonValue objValue = jsonObj.value(key);

      //qDebug()<<" key:"<<key<<" isDouble?:"<<objValue.isDouble();

      if(objValue.isString())
        mStrMap.insert(key,objValue.toString());

      else if(objValue.isDouble())
        mIntMap.insert(key,objValue.toDouble());

      else if(objValue.isArray())
        {
          QJsonArray arr = objValue.toArray();

          QList<QString> vec;
          vec.reserve(arr.size());

          for(int i=0; i<arr.size();i++)
            vec.insert(i,arr.at(i).toString());

          mArrMap.insert(key,vec);
        }

  }
 sizeof(double);
}
