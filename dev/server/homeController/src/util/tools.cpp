/*!
*
*   Birukus ( www.birukus.com )
*
*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
*
*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
*
*   Date: 09/2015
*
*/

#include "tools.h"
#include <dirent.h>
#include <math.h>

/**
 * @brief Gojo::Util::Tools::checkFileExists
 * @param aFilePath
 *
 * check if file exists
 */
void Gojo::Util::Tools::checkFileExists(QString aFilePath)
{

  QFileInfo checkFile(aFilePath);

  // check if file exists and if yes: Is it really a file and no directory?
  if (!checkFile.exists() || !checkFile.isFile() || !checkFile.isReadable()) {
      throwError(QString("Unable to open JSON file named :")+aFilePath);
  }

}

/**
 * @brief Gojo::Util::Tools::throwError
 * @param aMsg
 *
 * throw error
 */
void Gojo::Util::Tools::throwError(QString aMsg)
{
   QString msg("Gojo: encountered an Error: \n ");
   msg += aMsg;
   qDebug(aMsg.toStdString().c_str());

   throw;
      //qFatal(aMsg);
}


/**
 * does what printf does but returns a string instead
 *
 * it also uses % to represent, numbers and strings
 *
 * @param str
 * @param list
 * @return
 */
std::string Gojo::Util::Tools::prints( std::string str, std::initializer_list<double> list )
{
    std::string outstr;

    size_t oldIndex=0;
    size_t index = 0;
    for( auto elem : list )
    {
        /* Locate the substring to replace. */
        index = str.find("%", index);

        if (index == std::string::npos)
            break;

        outstr.append(str,oldIndex, index-oldIndex);
        char buffer [33];

        if(fmod(elem,1)==0)
            if(elem>0)
                sprintf(buffer,"%u",(unsigned long)elem);
            else
                sprintf(buffer,"%d",(signed long)elem);
        else
            sprintf(buffer,"%10.10f",(double)elem);

    //        if(elem>10000 && fmod(elem,1)==0)
    //            sprintf(buffer,"%lu",(unsigned long)elem);
    //        else
    //            sprintf(buffer,"%g",elem);
        outstr.append(buffer);

        oldIndex = index+1;
        index = oldIndex;

    }
    if(oldIndex<str.length())
        outstr.append(str,oldIndex, str.length()-oldIndex);
    return outstr;
}

/**
 * @brief getFilesInDirectory
 * @param aDirPath
 * @param aExtension
 * @return
 */
QList<QString> Gojo::Util::Tools::getFilesInDirectory(const QString aDirPath,const QString aExtension)
{
    QList<QString> filelist;

    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir (aDirPath.toStdString().c_str())) != NULL) {
      /* print all the files and directories within directory */
      while ((ent = readdir (dir)) != NULL)
      {
          // if the filenames contain the extension
        if(QString(ent->d_name).contains(aExtension))
            filelist.append(aDirPath + QString(ent->d_name));
      }
      // close the directory
      closedir (dir);
    } else {
      /* could not open directory */
      Gojo::Util::Tools::throwError(" Unable to get files from a directory :"+aDirPath);
    }

    return filelist;

}


