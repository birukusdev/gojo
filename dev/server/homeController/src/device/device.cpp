#include "device.h"

//using namespace Gojo::Devices;
//using namespace Gojo::Drivers;

// main constructor for Device
Gojo::Devices::Device::Device(QString xmlConfigFilePath)
{
//    parseXMLConfigFile(xmlConfigFilePath);

}

Gojo::Devices::Device::~Device()
{

    //TODO remove the input/output
//    m_DeviceNodes.clear();
}

///**
// * @brief Gojo::Devices::Device::parseXMLConfigFile
// *
// * reads a xml file and initializes the node map
// * @param xmlConfigFilePath
// * @return
// */
//bool Gojo::Devices::Device::parseXMLConfigFile(QString xmlConfigFilePath)
//{
//    QFile file(xmlConfigFilePath);
//    QString attr;
//    Node* nodePtr;
//    NodeTrigger* trigger;
//    NodeSlot* signal;

//    unsigned int inputNodeCounter=0;
//    unsigned int outputNodeCounter=0;

//    if(file.open(QIODevice::ReadOnly))
//    {
//        QXmlStreamReader xmlReader;

//        xmlReader.setDevice(&file);

//        QString node_Input_Or_Output( '-');


//        //Reading from the file
//        while (!xmlReader.isEndDocument())
//        {
//            xmlReader.readNext();

//            QString name = xmlReader.name().toString();

//            // if  device tag
//            if(name=="device" && xmlReader.isStartElement())
//            {
//               // going through each attribute

//               for(int n=0; n<xmlReader.attributes().size(); n++)
//               {
//                   QString att_name  = xmlReader.attributes().at(n).name().toString();
//                   QString att_value = xmlReader.attributes().at(n).value().toString();


//                    if(att_name.contains("name"))
//                        id.deviceName = att_value;
//                    else if(att_name.contains("homeid")) id.homeID = att_value.toInt();
//                    else if(att_name.contains("nodeid")) id.nodeID = att_value.toInt();
//                    else if(att_name.contains("type"))
//                    {
//                        if(att_value.contains("multi-sensor"))
//                            id.deviceType = ENVIRONMENT_SENSOR_DEVICE;
//                        else if(att_value.contains("motion-sensor"))
//                            id.deviceType = MOTION_SENSOR_DEVICE;
//                        else if(att_value.contains("light-switch"))
//                            id.deviceType = LIGHT_SWITCH_DEVICE;
//                        else if(att_value.contains("electrical-switch"))
//                            id.deviceType = ELECTRICAL_SWITCH_DEVICE;
//                        else if(att_value.contains("alarm"))
//                            id.deviceType = ALARM_DEVICE;
//                        else if(att_value.contains("speaker"))
//                            id.deviceType = SPEAKER_DEVICE;
//                        else if(att_value.contains("camera"))
//                            id.deviceType = CAMERA_DEVICE;
//                        else
//                            id.deviceType = UNKNOWN_DEVICE;
//                    }
//                    else if(att_name.contains("driver"))
//                    {
//                        if(att_value.contains("zwave"))
//                            id.driverType = ZWAVE_DRIVER;
//                        else if(att_value.contains("ipcam"))
//                            id.driverType = IPCAM_DRIVER;
//                        else if(att_value.contains("audio"))
//                            id.driverType = IPCAM_DRIVER;
//                        else
//                            id.driverType = UNKNOWN_DRIVER;
//                    }
//               }

//            }


//            /**
//              * Process the input nodes
//              *
//              ***/
//            // process the start of the node
//            if(name=="node" && xmlReader.isStartElement() )
//            {
//                // this marks a new node that has been read
//                inputNodeCounter++;

//                nodePtr = new Node();

//                nodePtr->id = id;

//                // going through each attribute

//                for(int n=0; n<xmlReader.attributes().size(); n++)
//                {
//                    QString att_name  = xmlReader.attributes().at(n).name().toString();
//                    QString att_value = xmlReader.attributes().at(n).value().toString();

//                    if(att_name.contains("datatype"))        nodePtr->id.dataType   = determineDataType(att_value);
//                    else if(att_name.contains("nodename"))   nodePtr->id.nodename   = att_value;
//                    else if(att_name.contains("classid"))    nodePtr->id.classID    = att_value.toInt();
//                    else if(att_name.contains("sensortype")) nodePtr->id.sensorType = att_value;
//                }

//            }

//            // process the end of the node
//            if(name=="node" && xmlReader.isEndElement() )
//            {
//                if(nodePtr!=nullptr)
//                    m_DeviceNodes[(*nodePtr).id.classID]=*nodePtr;


//                delete(nodePtr);
//            }

//            // process triggers in the input
//            if(name=="trigger" && xmlReader.isStartElement() )
//            {
//                trigger = new NodeTrigger();

//                trigger->id = nodePtr->id;

//                for(int n=0; n<xmlReader.attributes().size(); n++)
//                {
//                    QString att_name  = xmlReader.attributes().at(n).name().toString();
//                    QString att_value = xmlReader.attributes().at(n).value().toString();

//                    if     (att_name.contains("trigger_id"))     trigger->trigger_id     = att_value.toInt();
//                    else if(att_name.contains("trigger_label"))  trigger->trigger_label  = att_value;
//                    else if(att_name.contains("trigger_message"))trigger->trigger_message= att_value.toInt();
//                    else if(att_name.contains("description"))    trigger->description    = att_value;
//                    else if(att_name.contains("trigger_threshold_type"))    trigger->threshold_type      = determineDataType(att_value);
//                    else if(att_name.contains("trigger_threshold_value"))   trigger->trigger_threshold   = att_value;

//                    if(att_name.contains("trigger_threshold_test_approximation"))   trigger->testGreaterThan = (att_value.contains("greater"))? true:false;
//                    if(att_name.contains("trigger_threshold_test_approximation"))   trigger->testLessThan    = (att_value.contains("less"))? true:false;
//                }

//                nodePtr->mTriggers.push_back(*trigger);
//            }

//            // process signals in the input
//            if(name=="outputsignal" && xmlReader.isStartElement() )
//            {
//                signal = new NodeSlot();

//                signal->id = nodePtr->id;

//                for(int n=0; n<xmlReader.attributes().size(); n++)
//                {
//                    QString att_name  = xmlReader.attributes().at(n).name().toString();
//                    QString att_value = xmlReader.attributes().at(n).value().toString();

//                    if     (att_name.contains("signal_id"))     signal->signal_id        = att_value.toInt();
//                    else if(att_name.contains("signal_label"))  signal->signal_label     = att_value;
//                    else if(att_name.contains("signal_action_label"))signal->action_label= att_value;
//                    else if(att_name.contains("description"))    signal->description     = att_value;
//                    else if(att_name.contains("signal_type"))    signal->signal_type     = determineDataType(att_value);
//                    else if(att_name.contains("defaultvalue"))   signal->signal_value    = att_value;
//                }

//                nodePtr->mOutputSignals.push_back(*signal);
//            }


//        }
//        if (xmlReader.hasError())
//        {
//            qDebug()<< "XML error: " << xmlReader.errorString().data() ;
//        }
//    }
//}

///**
// * @brief Device::addDeviceData
// * @param data
// * @return
// */
//bool Device::addDeviceData( NodeID id, NodeData data)
//{
//    if(m_DeviceNodes.contains(id.classID))
//    {
//        QMap<int,Node>::iterator it = m_DeviceNodes.find(id.classID);

//        it.value().mInputData.append(data);

//        return true;
//    }
//    return false;
//}
