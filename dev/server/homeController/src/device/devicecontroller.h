/*!
*
*   Birukus ( www.birukus.com )
*
*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
*
*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
*
*   Date: 09/2015
*
*/
#ifndef DEVICECONTROLLER_H
#define DEVICECONTROLLER_H

#include "../util/singleton.h"
#include "../util/logger.h"
#include "../util/settings.h"
#include "../drivers/zwave/zwavedriver.h"

#include <pthread.h>

namespace Gojo{

    namespace Devices{


        /**
         * @brief The DeviceController class
         *
         * This class handles the interface between the device-managers and the rest of the program
         */
        class DeviceController
        {

        private: // attributes

            pthread_mutex_t m_procMutex; // allows one instance to alter the data

            //Gojo::Zwave::ZwaveController* zwave;

        private: // methods
            /**
             * @brief initThreads
             *
             * init thread related variables
             */
            void initThreads();

            /**
             * @brief initZWave
             *
             * init the zwave controller
             */
            void initZWave();

        public: //methods
            // main controller
            DeviceController();  //

            ~DeviceController();  //

            bool deviceStatusUpdate();

            bool registerDeviceStatusChangeCallBack(void* fcn);
        };


    }
}
#endif // DEVICECONTROLLER_H
