///*!
//*
//*   Birukus ( www.birukus.com )
//*
//*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
//*
//*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
//*
//*   Date: 09/2015
//*
//*/
//#ifndef NODETRIGGER_H
//#define NODETRIGGER_H

//#include "nodeDatatypes.h"

//namespace Gojo
//{

//namespace Devices
//{
//    using namespace Gojo::Devices;

//    /**
//     * @brief The NodeTrigger struct
//     *
//     * Encapsulates the trigger tests
//     *
//     * contains everything needed to evaluate a nodeData
//     */
//    class NodeTrigger
//    {
//    public:
//       NodeID id;

//       int trigger_id=0;

//       QString trigger_label="";
//       QString trigger_message="";
//       QString description="";

//       bool testLessThan;
//       bool testGreaterThan;

//       NodeDataType threshold_type;

//       QString trigger_threshold;

//    public:

//       void sendTriggerMessage()
//       {
//            printf("--- trigger happened!\n");
//       }

//       void print()
//       {
//        qDebug()<<"-- Node Trigger ----------";
//        id.print();
//        qDebug()<<" -------- trigger data ---";
//        qDebug()<<" triggerID: "<<trigger_id<<","<<" label:"<<trigger_label<<", message:"<<trigger_message<<", description:"<<description<<"\n        testLessThan:"
//               <<testLessThan<<" testGreaterThan:"<<testGreaterThan<<" data_type:"<<threshold_type<<" threshold:"<<trigger_threshold;
//        qDebug()<<"-----------";
//       }
//    };

//}

//}
//#endif // NODETRIGGER_H
