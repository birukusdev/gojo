///*!
//*
//*   Birukus ( www.birukus.com )
//*
//*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
//*
//*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
//*
//*   Date: 09/2015
//*
//*/
//#ifndef NODE_H
//#define NODE_H

//#include <QString>
//#include <QVector>
//#include <QXmlStreamReader>
//#include <QFile>
//#include <QDebug>

//#include <typeinfo>

//#include "nodeDatatypes.h"
//#include "nodetrigger.h"


//namespace Gojo
//{
//    namespace Devices
//    {


//    /**
//     * @brief The DeviceNode class
//     *
//     */
//    class Node
//    {

//    public:

//        //
//        // Input variables
//        //

//        // id - for device identification
//        NodeID id;

//        // this stores all the data that has been reported
//        QVector<NodeInputData>         mInputData;

//        // list of triggers
////        QVector<NodeTrigger>      mTriggers;

//        // list of output signals
//        QVector<NodeOutputData> mOutputSignals;

//    public:
//        Node();

//        // this takes in a template type data and writes it to the Node data
//        template<class T>
//        bool addSignalData(double time, T data)
//        {
//            NodeInputData d; d.set(time,data);

//            mInputData.append(d);

//            testTriggers(d);
//        }

//        void testTriggers(NodeInputData &data);

//        void print()
//        {
//            qDebug()<<"         <Node Printout> --------------";
//            id.print();
//            for(NodeInputData n : mInputData)
//                n.print();
//            qDebug()<<"         </Node Printout> --------------";
//        }

//        bool operator ==(const Node &n)
//        {
//            if(n.id.homeID == id.homeID && n.id.nodeID == id.nodeID)
//                return true;
//            else
//                return false;
//        }
//    };

//    }
//}
//#endif // NODE_H

