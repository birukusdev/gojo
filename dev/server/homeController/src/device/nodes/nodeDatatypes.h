///*!
//*
//*   Birukus ( www.birukus.com )
//*
//*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
//*
//*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
//*
//*   Date: 09/2015
//*
//*/
//#ifndef NODETYPES_H
//#define NODETYPES_H
//#include <QString>
//#include <QVector>
//#include <QXmlStreamReader>
//#include <QFile>
//#include <QDebug>
//#include <typeinfo>

//#include "../../drivers/driver.h"

//namespace Gojo
//{
//    namespace Devices
//    {

//        // used to determine the type of device
//        enum DeviceType{ UNKNOWN_DEVICE, ALARM_DEVICE, MOTION_SENSOR_DEVICE,
//                         LIGHT_SWITCH_DEVICE, ELECTRICAL_SWITCH_DEVICE,
//                         ENVIRONMENT_SENSOR_DEVICE, CAMERA_DEVICE, SPEAKER_DEVICE };


//        // used to determine the node data type
//        enum NodeDataType{ UNKNOWN_FORMAT, BOOL_FORMAT, INT_FORMAT, FLOAT_FORMAT,
//                           STRING_FORMAT, INT_ARRAY_FORMAT};
//        /**
//         * @brief determineDataType
//         * this function is used to convert type to enum
//         *
//         * @param type
//         * @return
//         */
//        static NodeDataType determineDataType(QString type)
//        {
//            if(type=="bool")
//                return BOOL_FORMAT;
//            else if(type=="int")
//                return INT_FORMAT;
//            else if(type=="float")
//                return FLOAT_FORMAT;
//            else if(type=="int-array")
//                return INT_ARRAY_FORMAT;
//            else
//                return UNKNOWN_FORMAT;
//        }

//        // encapsulates all id for a device
//        //
//        struct DeviceID
//        {
//        public:
//            QString    deviceName = "";
//            Gojo::Drivers::DriverType driverType = Gojo::Drivers::UNKNOWN_DRIVER;
//            DeviceType deviceType = UNKNOWN_DEVICE;

//            int homeID=0;
//            int nodeID=0;
//        };

//        // node id
//        struct NodeID : DeviceID
//        {

//            int classID=0;

//            NodeDataType dataType = UNKNOWN_FORMAT;

//            QString nodename = "";

//            QString sensorType = "";

//            NodeID(){}

//            void print()
//            {
//                qDebug()<<"             << NodeID Print:-----";
//                qDebug()<<"                 DeviceName:"<<deviceName<<" , DriverType:"<<driverType<<", DeviceType:"<<deviceType<<" homeID:"<<homeID<<" nodeID:"<<nodeID<<" classID:"<<classID;
//                qDebug()<<"                 -- Node Data --";
//                qDebug()<<"                 DataType:"<<dataType<<", NodeName:"<<nodename<<", SensorType:"<<sensorType;
//                qDebug()<<"             >> ";

//            }

//            NodeID operator= (const NodeID& id)
//            {
//                deviceName = id.deviceName;
//                driverType = id.driverType;
//                deviceType = id.deviceType;

//                homeID     = id.homeID;
//                nodeID     = id.nodeID;

//                classID    = id.classID;
//                dataType   = id.dataType;

//                nodename   = id.nodename;
//                sensorType = id.sensorType;

//                return *this;
//            }

//            NodeID& operator= (const DeviceID& id)
//            {
//                deviceName = id.deviceName;
//                driverType = id.driverType;
//                deviceType = id.deviceType;

//                homeID     = id.homeID;
//                nodeID     = id.nodeID;

//                return *this;
//            }

//            bool operator<(const NodeID& id) const
//            {
//                if(nodeID < id.nodeID)
//                    return true;
//                return false;
//            }

//            bool operator>(const NodeID& id) const
//            {
//                if(nodeID > id.nodeID)
//                    return true;
//                return false;
//            }

//            bool operator==(const NodeID& id) const
//            {
//                if(     deviceName == id.deviceName &&
//                        driverType == id.driverType &&
//                        deviceType == id.deviceType &&
//                        homeID     == id.homeID &&
//                        nodeID     == id.nodeID
//                        )
//                    return true;
//                return false;
//            }
//        };


//        struct NodeInputData
//        {
//            double observedTime;
//            NodeDataType dataType;

//            bool    bool_data;
//            int     int_data;
//            float   float_data;
//            QString string_data;

//            template<class T>
//            bool set(double time, T data)
//            {
//                if(typeid(T)==typeid(bool)){    observedTime=time;   bool_data=data; dataType = BOOL_FORMAT;}
//                if(typeid(T)==typeid(int)){     observedTime=time;    int_data=data; dataType = INT_FORMAT; }
//                if(typeid(T)==typeid(float)){   observedTime=time;  float_data=data; dataType = FLOAT_FORMAT;}
//                if(typeid(T)==typeid(QString)){ observedTime=time; string_data=data; dataType = STRING_FORMAT;}

//                return true;
//            }

//            void print()
//            {
//                qDebug()<<"                 //NodeData// type:"<<dataType<<" Value:{ bool:"<<bool_data<<", int:"<<int_data<<" float:"<<float_data<<" str:"<<string_data;
//            }
//        };


//        /**
//         * @brief The NodeSignal struct
//         *
//         * This contains all the input and output values
//         */
//        class NodeOutputData
//        {
//        public:
//           NodeID id;

//           int signal_id=0;

//           QString signal_label="";
//           QString action_label="";
//           QString description="";

//           NodeDataType signal_type;

//           QString signal_value;

//        public:
//           template<class T>
//           bool test(T testvalue)
//           { }

//           void sendTriggerMessage()
//           { }


//           void print()
//           {
//            qDebug()<<"-- Node Output ----------";
//            id.print();
//            qDebug()<<" -------- Output data ---";
//            qDebug()<<" signalID: "<<signal_id<<","<<" label:"<<signal_label<<", action_label:"<<action_label<<", description:"<<description<<
//                      " signal_type:"<<signal_type<<" signal_value:"<<signal_value;
//            qDebug()<<"-----------";
//           }
//        };




//    }

//}

//#endif // NODETYPES_H

