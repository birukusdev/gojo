/*!
*
*   Birukus ( www.birukus.com )
*
*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
*
*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
*
*   Date: 09/2015
*
*/
#ifndef DEVICES_H
#define DEVICES_H

#include <QString>
#include <QVector>
#include <QXmlStreamReader>
#include <QFile>
#include <QDebug>
#include <typeinfo>
#include <map>

#include "nodes/nodeDatatypes.h"
#include "nodes/nodetrigger.h"

#include "nodes/node.h"

//#include "../drivers/driver.h"

namespace Gojo
{
    namespace Devices
    {

        /**
         * @brief The Device class
         *
         * This class encapsulates the functionality of devices on the network
         */
        class Device
        {
        private:

//            Gojo::Devices::DeviceID id;

            // a map of the device nodes by class id
//            QMap<int,Node> m_DeviceNodes; //

        private:

            bool parseXMLConfigFile(QString xmlConfigFilePath);

        public:

            // initialize from an xml config file
            Device(QString xmlConfigFilePath);

            ~Device();

            /**
             * Used by drivers to push data
             * This is a template function that will push data to the queue
             */
//            bool addDeviceData( NodeID id, NodeInputData data);

         public:
//            int getNodeSize(){ return m_DeviceNodes.size(); }

//            Node getNodeFromIndex(unsigned int ind)
//            {
//                if(ind<m_DeviceNodes.size())
//                {
//                    return m_DeviceNodes[ m_DeviceNodes.keys().at(ind) ];
//                }
//                return Node();
//            }

//            void print()
//            {
////                int size = m_DeviceNodes.size();

////                qDebug()<<"<Device> -- Print -- node size="<<size<<"-------";

////                for (auto const& n : m_DeviceNodes.keys())
////                {
////                    qDebug()<<"     <node>------\n key-------------------------";
////                    qDebug()<<"             key="<<n;


////                    m_DeviceNodes[n].print();
////                    qDebug()<<"     </node>--------------";
////                }
////                qDebug()<<"</debug>";
//            }
        };


    }
}

#endif // DEVICE_H
