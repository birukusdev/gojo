
/*!
*
*   Birukus ( www.birukus.com )
*
*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
*
*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
*
*   Date: 09/2015
*
*/

#include "unittest.h"
#include "../../src/device/device.h"
#include "../../src/device/node.h"

using namespace Gojo::Test;


/**
 * @brief UnitTest::test_device
 *
 * Tests the Device class defined in device.h
 *
 * TEST1: is the xml device file loaded correctly?
 */

void UnitTest::test_device()
{
    QString devicefile = "/Birukus/Dev/Birukus/dev/gojo/gitmaster/dev/server/cpp/test/data/testdata1/config/devices/device1.xml";
    // create a new device

    Gojo::Devices::Device device1(devicefile);

    ASSERT_TRUE((device1.getNodeSize()>0), "Test: Device file read")

    int nodesize= device1.getNodeSize();

    ASSERT_EQUALS(nodesize,3," Test: Device file read nodes ")

    device1.print();

}

/**
 * @brief UnitTest::test_device_node_signal_add
 *
 * Test1: can the device class add signals for nodes?
 */
void UnitTest::test_device_node_signal_add()
{
    // device file
    QString devicefile = "/Birukus/Dev/Birukus/dev/gojo/gitmaster/dev/server/cpp/test/data/testdata1/config/devices/device1.xml";
    // create a new device

    Gojo::Devices::Device device1(devicefile);

    Gojo::Devices::NodeID node;
    node.classID = 5;
    node.dataType = Gojo::Devices::INT_FORMAT;
    node.deviceName = "sensor1";
    node.homeID = 2094;
    node.nodeID = 2;

    Gojo::Devices::NodeData data;
    data.observedTime = 2039409234.0;
    data.dataType = Gojo::Devices::INT_FORMAT;
    data.string_data = "666";
    data.int_data = 666;



    ASSERT_TRUE(device1.addDeviceData(node,data), "Test: Device added a node")

    node.classID = 2000000;
    ASSERT_TRUE(device1.addDeviceData(node,data), "Test2: Device added a node")

    int nodesize= device1.getNodeSize();

//    device1.print();

}
