/*!
*
*   Birukus ( www.birukus.com )
*
*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
*
*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
*
*   Date: 09/2015
*
*/
#ifndef TEST
#define TEST
#include <QString>

# define ASSERT_TRUE(x,msg) \
    { \
        if(x) \
           test_pass_print(msg);\
        else\
            test_fail_print(msg);\
    }
# define ASSERT_EQUALS(x,y,msg) \
    { \
        test_equals(x,y,msg); \
    }

# define THROWS(X,msg) \
    { \
        try{ X; test_pass_print(msg); } \
        catch(...){ test_fail_print(msg); }\
    }




static void test_pass_print(QString msg){

    fprintf(stderr, "\033[1;32m Pass: '%s' \033[0m\n", msg.toStdString().c_str());
}

static void test_fail_print(QString msg){

    fprintf(stderr, "\033[1;31m Fail: '%s' \033[0m\n", msg.toStdString().c_str());
}

template<class T>
static bool test_equals(const T &a,const T &b, QString msg){
    if(a==b)
        test_pass_print(msg);
    else
        test_fail_print(msg);
}

#endif // TEST

