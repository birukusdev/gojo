/*!
*
*   Birukus ( www.birukus.com )
*
*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
*
*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
*
*   Date: 09/2015
*
*/
#include "ui/testmainwindow.h"
#include "unittest.h"
//#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    TestMainWindow w;
    w.show();

//    return a.exec();

    printf("test\n");

    Gojo::Test::UnitTest ut;

//    ut.test1();

    ut.test_device_node_signal_add();

//    Gojo::Test::UnitTest t;

//    t.initTestCase();

//    t.myFirstTest();
    return 0;
}
