/*!
*
*   Birukus ( www.birukus.com )
*
*   Gojo - Zwave Based house automation software  (gojo.birukus.com)
*
*   Author: Biruh Tesfaye ( btesfaye@birukus.com )
*
*   Date: 09/2015
*
*/

#ifndef TEST_H
#define TEST_H

#include <QtTest/QtTest>
#include <QString>
#include <QObject>

#include "test.h"

namespace Gojo
{
    namespace Test
    {
        class UnitTest
        {
        private:


        public:
            UnitTest();
            ~UnitTest();

            void test1();

            void test_device();
            void test_device_node_signal_add();
        };
    }
}

#endif // TEST_H
