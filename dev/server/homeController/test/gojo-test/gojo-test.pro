#-------------------------------------------------
#
#  GOJO : test
#
#-------------------------------------------------

QT       += core gui xml testlib

TEMPLATE = app

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

####### Third Party Libraries #################
ZWAVEPATH =/Birukus/Dev/Birukus/dev/gojo/gitmaster/lib/share/open-zwave-build
INCLUDEPATH += -I $${ZWAVEPATH}/include/openzwave
INCLUDEPATH += -I $${ZWAVEPATH}/include
LIBS        += -L$${ZWAVEPATH}/lib64 -lopenzwave -Wl,-Bdynamic
QMAKE_RPATHDIR += /Birukus/Dev/Birukus/dev/gojo/gitmaster/lib/open-zwave/debug/lib64
#############################
TARGET_LINK_LIBRARIES += ${QT_LIBRARIES} ${QT_QTTEST_LIBRARY}
TARGET = gojo-test # output executable path

#############################

CONFIG += c++11

#############################
HEADERS +=  ../../src/ui/mainwindow.h \
            ../../src/util/logger.h \
            ../../src/util/settings.h \
            ../../src/util/singleton.h \
            ../../src/drivers/zwave/zwavecontroller.h \
            ../../src/util/error.h \
            ../../src/util/tools.h \
            ../../src/util/macros.h \
            ../../src/main.h \
            ../../src/device/devicecontroller.h \
            ../../src/device/device.h \
            ../../src/device/node.h \
            ../../src/device/nodeoutputsignal.h \
            ../../src/device/nodetrigger.h \
            ../../src/device/nodeDatatypes.h \
            src/unittest.h \
            src/ui/testmainwindow.h \
    src/test.h

SOURCES +=  src/main.cpp \
            ../../src/drivers/zwave/zwave.cpp \
            ../../src/drivers/zwave/zwave_test2.cpp \
            ../../src/util/settings.cpp \
            ../../src/run.cpp \
            ../../src/util/error.cpp \
            ../../src/util/tools.cpp \
            ../../src/util/logger.cpp \
            ../../src/drivers/zwave/zwavecontroller.cpp \
            ../../src/device/devicecontroller.cpp \
            ../../src/device/device.cpp \
            ../../src/device/node.cpp \
            ../../src/device/nodeoutputsignal.cpp \
            ../../src/device/nodetrigger.cpp \
            ../../src/ui/mainwindow.cpp \
            src/unittest.cpp \
            src/ui/testmainwindow.cpp \
    src/test_device.cpp

FORMS   += src/ui/testmainwindow.ui

DESTDIR  = bin

MOC_DIR  = build

RCC_DIR  = build



