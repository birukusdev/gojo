#-------------------------------------------------
#
#  GOJO :
#
#-------------------------------------------------
##### USER VARIABLES ##########
PROJ_DIR=/Users/btesfaye/Birukus/dev/gojo/gitmaster/dev/server/homeController
ZWAVE_DIR=/Users/btesfaye/Birukus/dev/gojo/gitmaster/lib/share/open-zwave-osx
###############################

QT       += core gui xml

TEMPLATE = app

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

####### Third Party Libraries #################

INCLUDEPATH += -I $${ZWAVE_DIR}/include/openzwave
INCLUDEPATH += -I $${ZWAVE_DIR}/include
LIBS += -L$${ZWAVE_DIR}/lib64 -lopenzwave
#QMAKE_RPATHDIR += $${ZWAVE_DIR}/lib64
QMAKE_RPATHDIR +=/Users/btesfaye/Qt/5.5/clang_64/lib
#QMAKE_CXXFLAGS += -Wl,-Bdynamic
#############################

TARGET = gojo # output executable path

#############################

CONFIG += c++11

#############################
HEADERS +=  $${PROJ_DIR}/src/drivers/ui/gui/mainwindow.h \
            $${PROJ_DIR}/src/util/logger.h \
            $${PROJ_DIR}/src/util/settings.h \
            $${PROJ_DIR}/src/util/singleton.h \
            $${PROJ_DIR}/src/util/error.h \
            $${PROJ_DIR}/src/util/tools.h \
            $${PROJ_DIR}/src/util/macros.h \
            $${PROJ_DIR}/src/main.h \
            $${PROJ_DIR}/src/device/device.h \
            $${PROJ_DIR}/src/device/nodes/node.h \
            $${PROJ_DIR}/src/device/nodes/nodetrigger.h \
            $${PROJ_DIR}/src/device/nodes/nodeDatatypes.h \
            $${PROJ_DIR}/src/controllers/events/eventprocessor.h \
            $${PROJ_DIR}/src/controllers/controller.h \
            $${PROJ_DIR}/src/drivers/driver.h \
            $${PROJ_DIR}/src/drivers/zwave/zwavedriver.h

SOURCES += $${PROJ_DIR}/src/main.cpp \
            $${PROJ_DIR}/src/drivers/ui/gui/mainwindow.cpp \
            $${PROJ_DIR}/src/drivers/zwave/zwave.cpp \
            $${PROJ_DIR}/src/drivers/zwave/zwave_test2.cpp \
            $${PROJ_DIR}/src/util/settings.cpp \
            $${PROJ_DIR}/src/util/error.cpp \
            $${PROJ_DIR}/src/util/tools.cpp \
            $${PROJ_DIR}/src/util/logger.cpp \
            $${PROJ_DIR}/src/drivers/zwave/zwavecontroller.cpp \
            $${PROJ_DIR}/src/device/devicecontroller.cpp \
            $${PROJ_DIR}/src/device/device.cpp \
            $${PROJ_DIR}/src/controllers/events/eventprocessor.cpp \
            $${PROJ_DIR}/src/controllers/controller.cpp

FORMS   += $${PROJ_DIR}/src/drivers/ui/gui/mainwindow.ui

DESTDIR = $${PROJ_DIR}/bin/

OBJECTS_DIR = $${PROJ_DIR}/build/obj/.obj
MOC_DIR = $${PROJ_DIR}/build/moc/.moc
RCC_DIR = $${PROJ_DIR}/build/rcc/.rcc
UI_DIR = $${PROJ_DIR}/build/ui/.ui
